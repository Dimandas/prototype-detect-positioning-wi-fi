﻿Для компиляции программы необходимо определить три глобальные переменные на собранные библиотеки:

32:
WXWIN_8Iv7V_32       - wxWidgets
SerialWIN_8Iv7V_32   - Serial
GeoWIN_8Iv7V_32      - GeographicLib

64:
WXWIN_8Iv7V_64       - wxWidgets
SerialWIN_8Iv7V_64   - Serial
GeoWIN_8Iv7V_64      - GeographicLib

wxWidgets - https://github.com/wxWidgets/wxWidgets
Версия на момент разработки: 3.1.3

Serial Communication Library - https://github.com/wjwwood/serial

GeographicLib - https://sourceforge.net/projects/geographiclib/
Версия на момент разработки: 1.50