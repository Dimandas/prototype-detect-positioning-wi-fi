﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "main.h"

#include <wx/app.h>
#include <wx/msgdlg.h>
#include <wx/clipbrd.h>

#include "dialogs/gps.h"
#include "dialogs/wifi.h"
#include "dialogs/net.h"
#include "dialogs/wifiadapter.h"
#include "dialogs/inputcoord.h"
#include "dialogs/averagedata.h"

#include "../gps/parse.h"

#include "../math/trilateration.h"

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)   // костыль, api wx сокет не поддерживает bind/connect
EVT_SOCKET(41, MainFrame::OnSocketEvent1)
EVT_SOCKET(42, MainFrame::OnSocketEvent2)
EVT_SOCKET(43, MainFrame::OnSocketEvent3)
EVT_SOCKET(44, MainFrame::OnSocketEvent4)
wxEND_EVENT_TABLE()

MainFrame::MainFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	this->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer(wxVERTICAL);

	m_staticText1 = new wxStaticText(this, wxID_ANY, wxT("Данные по GPS"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText1->Wrap(-1);
	m_staticText1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
	m_staticText1->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	bSizer7->Add(m_staticText1, 0, wxALL, 5);

	m_staticTextCOMPort = new wxStaticText(this, wxID_ANY, wxT("COM порт: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextCOMPort->Wrap(-1);
	m_staticTextCOMPort->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer7->Add(m_staticTextCOMPort, 0, wxALL, 5);

	m_staticTextBaudrate = new wxStaticText(this, wxID_ANY, wxT("Скорость передачи: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextBaudrate->Wrap(-1);
	m_staticTextBaudrate->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer7->Add(m_staticTextBaudrate, 0, wxALL, 5);

	m_staticTextLatitude = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLatitude->Wrap(-1);
	m_staticTextLatitude->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer7->Add(m_staticTextLatitude, 0, wxALL, 5);

	m_staticTextLongitude = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLongitude->Wrap(-1);
	m_staticTextLongitude->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer7->Add(m_staticTextLongitude, 0, wxALL, 5);

	m_staticTextAltitude = new wxStaticText(this, wxID_ANY, wxT("Высота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAltitude->Wrap(-1);
	m_staticTextAltitude->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer7->Add(m_staticTextAltitude, 0, wxALL, 5);


	bSizer6->Add(bSizer7, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer(wxVERTICAL);

	m_staticText11 = new wxStaticText(this, wxID_ANY, wxT("Данные по Wi-Fi"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText11->Wrap(-1);
	m_staticText11->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer9->Add(m_staticText11, 0, wxALL, 5);

	m_staticTextWiFiAdapter = new wxStaticText(this, wxID_ANY, wxT("Адаптер: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextWiFiAdapter->Wrap(-1);
	m_staticTextWiFiAdapter->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer9->Add(m_staticTextWiFiAdapter, 0, wxALL, 5);

	m_staticTextSSID = new wxStaticText(this, wxID_ANY, wxT("SSID: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextSSID->Wrap(-1);
	m_staticTextSSID->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer9->Add(m_staticTextSSID, 0, wxALL, 5);

	m_staticTextMAC = new wxStaticText(this, wxID_ANY, wxT("MAC: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextMAC->Wrap(-1);
	m_staticTextMAC->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer9->Add(m_staticTextMAC, 0, wxALL, 5);

	m_staticTextPWR = new wxStaticText(this, wxID_ANY, wxT("PWR: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextPWR->Wrap(-1);
	m_staticTextPWR->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer9->Add(m_staticTextPWR, 0, wxALL, 5);

	m_staticTextDistance = new wxStaticText(this, wxID_ANY, wxT("Расстояние: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextDistance->Wrap(-1);
	m_staticTextDistance->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer9->Add(m_staticTextDistance, 0, wxALL, 5);


	bSizer6->Add(bSizer9, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer(wxVERTICAL);

	m_staticText5 = new wxStaticText(this, wxID_ANY, wxT("Данные по сети"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	m_staticText5->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer8->Add(m_staticText5, 0, wxALL, 5);

	m_staticTextComp1 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 1: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextComp1->Wrap(-1);
	m_staticTextComp1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer8->Add(m_staticTextComp1, 0, wxALL, 5);

	m_staticTextComp2 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 2: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextComp2->Wrap(-1);
	m_staticTextComp2->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer8->Add(m_staticTextComp2, 0, wxALL, 5);

	m_staticTextComp3 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 3: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextComp3->Wrap(-1);
	m_staticTextComp3->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer8->Add(m_staticTextComp3, 0, wxALL, 5);

	/*
	m_staticTextComp4 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 4: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextComp4->Wrap(-1);
	m_staticTextComp4->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer8->Add(m_staticTextComp4, 0, wxALL, 5); */

	m_staticTextServer = new wxStaticText(this, wxID_ANY, wxT("Сервер: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextServer->Wrap(-1);
	m_staticTextServer->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer8->Add(m_staticTextServer, 0, wxALL, 5);


	bSizer6->Add(bSizer8, 1, wxEXPAND, 5);

	bSizer11 = new wxBoxSizer(wxVERTICAL);

	m_staticText16 = new wxStaticText(this, wxID_ANY, wxT("Найденные координаты"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText16->Wrap(-1);
	m_staticText16->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticText16, 0, wxALL, 5);

	m_staticTextLatitudeOut = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLatitudeOut->Wrap(-1);
	m_staticTextLatitudeOut->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticTextLatitudeOut, 0, wxALL, 5);

	m_staticTextLongitudeOut = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLongitudeOut->Wrap(-1);
	m_staticTextLongitudeOut->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticTextLongitudeOut, 0, wxALL, 5);

	/*
	m_staticTextAltitudeOut = new wxStaticText(this, wxID_ANY, wxT("Высота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAltitudeOut->Wrap(-1);
	m_staticTextAltitudeOut->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticTextAltitudeOut, 0, wxALL, 5); */

	m_staticTextAzimuth = new wxStaticText(this, wxID_ANY, wxT("Азимут: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAzimuth->Wrap(-1);
	m_staticTextAzimuth->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticTextAzimuth, 0, wxALL, 5);

	m_staticTextCoordDistance = new wxStaticText(this, wxID_ANY, wxT("Расстояние: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextCoordDistance->Wrap(-1);
	m_staticTextCoordDistance->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticTextCoordDistance, 0, wxALL, 5);

	m_staticTextAllStatus = new wxStaticText(this, wxID_ANY, wxT("Состояние: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAllStatus->Wrap(-1);
	m_staticTextAllStatus->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer11->Add(m_staticTextAllStatus, 0, wxALL, 5);

	m_copy = new wxButton(this, wxID_ANY, wxT("Копировать"), wxDefaultPosition, wxDefaultSize, 0);
	m_copy->Enable(false);

	bSizer11->Add(m_copy, 0, wxALL, 5);

	m_hyperlinkShowMap = new wxHyperlinkCtrl(this, wxID_ANY, wxT("Посмотреть на карте OpenStreetMap"), wxT("https://www.openstreetmap.org/#map=5/-73.659/64.336"), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
	m_hyperlinkShowMap->Enable(false);

	bSizer11->Add(m_hyperlinkShowMap, 0, wxALL, 5);


	bSizer6->Add(bSizer11, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer6, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer2, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer(wxVERTICAL);


	bSizer12->Add(0, 0, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer12, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer91;
	bSizer91 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer111;
	bSizer111 = new wxBoxSizer(wxVERTICAL);

	m_staticText20 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 1"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText20->Wrap(-1);
	m_staticText20->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticText20, 0, wxALL, 5);

	m_staticTextLatitudeCOMP1 = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLatitudeCOMP1->Wrap(-1);
	m_staticTextLatitudeCOMP1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextLatitudeCOMP1, 0, wxALL, 5);

	m_staticTextLongitudeCOMP1 = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLongitudeCOMP1->Wrap(-1);
	m_staticTextLongitudeCOMP1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextLongitudeCOMP1, 0, wxALL, 5);

	m_staticTextAltitudeCOMP1 = new wxStaticText(this, wxID_ANY, wxT("Высота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAltitudeCOMP1->Wrap(-1);
	m_staticTextAltitudeCOMP1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextAltitudeCOMP1, 0, wxALL, 5);

	m_staticTextSSIDCOMP1 = new wxStaticText(this, wxID_ANY, wxT("SSID: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextSSIDCOMP1->Wrap(-1);
	m_staticTextSSIDCOMP1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextSSIDCOMP1, 0, wxALL, 5);

	m_staticTextMAC1 = new wxStaticText(this, wxID_ANY, wxT("MAC: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextMAC1->Wrap(-1);
	m_staticTextMAC1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextMAC1, 0, wxALL, 5);

	m_staticTextPWRCOMP1 = new wxStaticText(this, wxID_ANY, wxT("PWR: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextPWRCOMP1->Wrap(-1);
	m_staticTextPWRCOMP1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextPWRCOMP1, 0, wxALL, 5);

	m_staticTextDistance1 = new wxStaticText(this, wxID_ANY, wxT("Расстояние: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextDistance1->Wrap(-1);
	m_staticTextDistance1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer111->Add(m_staticTextDistance1, 0, wxALL, 5);


	bSizer10->Add(bSizer111, 1, wxEXPAND, 5);


	bSizer10->Add(0, 0, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer1111;
	bSizer1111 = new wxBoxSizer(wxVERTICAL);

	m_staticText201 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 2"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText201->Wrap(-1);
	m_staticText201->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticText201, 0, wxALL, 5);

	m_staticTextLatitudeCOMP11 = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLatitudeCOMP11->Wrap(-1);
	m_staticTextLatitudeCOMP11->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextLatitudeCOMP11, 0, wxALL, 5);

	m_staticTextLongitudeCOMP11 = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLongitudeCOMP11->Wrap(-1);
	m_staticTextLongitudeCOMP11->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextLongitudeCOMP11, 0, wxALL, 5);

	m_staticTextAltitudeCOMP11 = new wxStaticText(this, wxID_ANY, wxT("Высота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAltitudeCOMP11->Wrap(-1);
	m_staticTextAltitudeCOMP11->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextAltitudeCOMP11, 0, wxALL, 5);

	m_staticTextSSIDCOMP11 = new wxStaticText(this, wxID_ANY, wxT("SSID: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextSSIDCOMP11->Wrap(-1);
	m_staticTextSSIDCOMP11->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextSSIDCOMP11, 0, wxALL, 5);

	m_staticTextMAC2 = new wxStaticText(this, wxID_ANY, wxT("MAC: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextMAC2->Wrap(-1);
	m_staticTextMAC2->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextMAC2, 0, wxALL, 5);

	m_staticTextPWRCOMP11 = new wxStaticText(this, wxID_ANY, wxT("PWR: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextPWRCOMP11->Wrap(-1);
	m_staticTextPWRCOMP11->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextPWRCOMP11, 0, wxALL, 5);

	m_staticTextDistance2 = new wxStaticText(this, wxID_ANY, wxT("Расстояние: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextDistance2->Wrap(-1);
	m_staticTextDistance2->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1111->Add(m_staticTextDistance2, 0, wxALL, 5);


	bSizer10->Add(bSizer1111, 1, wxEXPAND, 5);


	bSizer10->Add(0, 0, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer1112;
	bSizer1112 = new wxBoxSizer(wxVERTICAL);

	m_staticText202 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 3"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText202->Wrap(-1);
	m_staticText202->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticText202, 0, wxALL, 5);

	m_staticTextLatitudeCOMP12 = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLatitudeCOMP12->Wrap(-1);
	m_staticTextLatitudeCOMP12->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextLatitudeCOMP12, 0, wxALL, 5);

	m_staticTextLongitudeCOMP12 = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLongitudeCOMP12->Wrap(-1);
	m_staticTextLongitudeCOMP12->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextLongitudeCOMP12, 0, wxALL, 5);

	m_staticTextAltitudeCOMP12 = new wxStaticText(this, wxID_ANY, wxT("Высота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAltitudeCOMP12->Wrap(-1);
	m_staticTextAltitudeCOMP12->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextAltitudeCOMP12, 0, wxALL, 5);

	m_staticTextSSIDCOMP12 = new wxStaticText(this, wxID_ANY, wxT("SSID: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextSSIDCOMP12->Wrap(-1);
	m_staticTextSSIDCOMP12->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextSSIDCOMP12, 0, wxALL, 5);

	m_staticTextMAC3 = new wxStaticText(this, wxID_ANY, wxT("MAC: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextMAC3->Wrap(-1);
	m_staticTextMAC3->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextMAC3, 0, wxALL, 5);

	m_staticTextPWRCOMP12 = new wxStaticText(this, wxID_ANY, wxT("PWR: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextPWRCOMP12->Wrap(-1);
	m_staticTextPWRCOMP12->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextPWRCOMP12, 0, wxALL, 5);

	m_staticTextDistance3 = new wxStaticText(this, wxID_ANY, wxT("Расстояние: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextDistance3->Wrap(-1);
	m_staticTextDistance3->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1112->Add(m_staticTextDistance3, 0, wxALL, 5);


	bSizer10->Add(bSizer1112, 1, wxEXPAND, 5);

	/*
	wxBoxSizer* bSizer1113;
	bSizer1113 = new wxBoxSizer(wxVERTICAL);

	m_staticText203 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 4"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText203->Wrap(-1);
	m_staticText203->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1113->Add(m_staticText203, 0, wxALL, 5);

	m_staticTextLatitudeCOMP13 = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLatitudeCOMP13->Wrap(-1);
	m_staticTextLatitudeCOMP13->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1113->Add(m_staticTextLatitudeCOMP13, 0, wxALL, 5);

	m_staticTextLongitudeCOMP13 = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextLongitudeCOMP13->Wrap(-1);
	m_staticTextLongitudeCOMP13->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1113->Add(m_staticTextLongitudeCOMP13, 0, wxALL, 5);

	m_staticTextAltitudeCOMP13 = new wxStaticText(this, wxID_ANY, wxT("Высота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextAltitudeCOMP13->Wrap(-1);
	m_staticTextAltitudeCOMP13->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1113->Add(m_staticTextAltitudeCOMP13, 0, wxALL, 5);

	m_staticTextSSIDCOMP13 = new wxStaticText(this, wxID_ANY, wxT("SSID: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextSSIDCOMP13->Wrap(-1);
	m_staticTextSSIDCOMP13->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1113->Add(m_staticTextSSIDCOMP13, 0, wxALL, 5);

	m_staticTextMAC4 = new wxStaticText( this, wxID_ANY, wxT("MAC: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextMAC4->Wrap( -1 );
	m_staticTextMAC4->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );

	bSizer1113->Add( m_staticTextMAC4, 0, wxALL, 5 );

	m_staticTextPWRCOMP13 = new wxStaticText(this, wxID_ANY, wxT("PWR: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextPWRCOMP13->Wrap(-1);
	m_staticTextPWRCOMP13->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));

	bSizer1113->Add(m_staticTextPWRCOMP13, 0, wxALL, 5);

	m_staticTextDistance4 = new wxStaticText( this, wxID_ANY, wxT("Расстояние: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextDistance4->Wrap( -1 );
	m_staticTextDistance4->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );

	bSizer1113->Add( m_staticTextDistance4, 0, wxALL, 5 );


	bSizer10->Add(bSizer1113, 1, wxEXPAND, 5);
	*/


	bSizer91->Add(bSizer10, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer91, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer(wxVERTICAL);


	bSizer13->Add(0, 0, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer13, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);


	bSizer3->Add(0, 0, 1, wxEXPAND, 5);

	m_buttonStart = new wxButton(this, wxID_ANY, wxT("Запустить"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer3->Add(m_buttonStart, 0, wxALL | wxEXPAND, 5);

	m_buttonStop = new wxButton(this, wxID_ANY, wxT("Остановить"), wxDefaultPosition, wxDefaultSize, 0);
	m_buttonStop->Enable(false);

	bSizer3->Add(m_buttonStop, 0, wxALL | wxEXPAND, 5);


	bSizer1->Add(bSizer3, 1, wxEXPAND, 5);


	this->SetSizer(bSizer1);
	this->Layout();
	m_menubar1 = new wxMenuBar(0);
	m_menuGPS = new wxMenu();
	m_menuItemSelectGPS = new wxMenuItem(m_menuGPS, wxID_ANY, wxString(wxT("Выбрать GPS адаптер")), wxEmptyString, wxITEM_NORMAL);
	m_menuGPS->Append(m_menuItemSelectGPS);

	m_menuGPS->AppendSeparator();

	wxMenuItem* m_menuItemInputCoord;
	m_menuItemInputCoord = new wxMenuItem(m_menuGPS, wxID_ANY, wxString(wxT("Ввести координаты")), wxEmptyString, wxITEM_NORMAL);
	m_menuGPS->Append(m_menuItemInputCoord);

	m_menubar1->Append(m_menuGPS, wxT("GPS"));

	m_menuWiFi = new wxMenu();
	m_menuItemSelectWiFiAdapter = new wxMenuItem(m_menuWiFi, wxID_ANY, wxString(wxT("Выбрать Wi-Fi адаптер")), wxEmptyString, wxITEM_NORMAL);
	m_menuWiFi->Append(m_menuItemSelectWiFiAdapter);

	m_menuItemSelectWiFi = new wxMenuItem(m_menuWiFi, wxID_ANY, wxString(wxT("Выбрать Wi-Fi точку")), wxEmptyString, wxITEM_NORMAL);
	m_menuWiFi->Append(m_menuItemSelectWiFi);

	m_menubar1->Append(m_menuWiFi, wxT("Wi-Fi"));

	m_menuNet = new wxMenu();
	wxMenuItem* m_menuItemAddComp;
	m_menuItemAddComp = new wxMenuItem(m_menuNet, wxID_ANY, wxString(wxT("Добавить компьютер")), wxEmptyString, wxITEM_NORMAL);
	m_menuNet->Append(m_menuItemAddComp);

	m_menubar1->Append(m_menuNet, wxT("Сеть"));

	m_menuSettings = new wxMenu();
	wxMenuItem* m_menuItemAverData;
	m_menuItemAverData = new wxMenuItem(m_menuSettings, wxID_ANY, wxString(wxT("Усреднение данных")), wxEmptyString, wxITEM_NORMAL);
	m_menuSettings->Append(m_menuItemAverData);

	m_menubar1->Append(m_menuSettings, wxT("Настройки"));

	m_help = new wxMenu();
	wxMenuItem* m_about;
	m_about = new wxMenuItem(m_help, wxID_ANY, wxString(wxT("О программе")), wxEmptyString, wxITEM_NORMAL);
	m_help->Append(m_about);

	m_menubar1->Append(m_help, wxT("Справка"));

	this->SetMenuBar(m_menubar1);


	this->Centre(wxBOTH);

	// Add user code
	// костыль
	m_TextCOMPort = m_staticTextCOMPort->GetLabel();
	m_TextBaudrate = m_staticTextBaudrate->GetLabel();
	m_TextLatitude = m_staticTextLatitude->GetLabel();
	m_TextLongitude = m_staticTextLongitude->GetLabel();
	m_TextAltitude = m_staticTextAltitude->GetLabel();

	m_TextAdapter = m_staticTextWiFiAdapter->GetLabel();
	m_TextSSID = m_staticTextSSID->GetLabel();
	m_TextMAC = m_staticTextMAC->GetLabel();
	m_TextPWR = m_staticTextPWR->GetLabel();
	m_TextDistance = m_staticTextDistance->GetLabel();

	m_TextComp1 = m_staticTextComp1->GetLabel();
	m_TextComp2 = m_staticTextComp2->GetLabel();
	m_TextComp3 = m_staticTextComp3->GetLabel();
	//m_TextComp4 = m_staticTextComp4->GetLabel();
	m_TextServer = m_staticTextServer->GetLabel();

	m_TextLatitudeOut = m_staticTextLatitudeOut->GetLabel();
	m_TextLongitudeOut = m_staticTextLongitudeOut->GetLabel();
	//m_TextAltitudeOut = m_staticTextAltitudeOut->GetLabel();
	m_TextAzimuth = m_staticTextAzimuth->GetLabel();
	m_TextAllStatus = m_staticTextAllStatus->GetLabel();

	m_Textlatitude1 = m_staticTextLatitudeCOMP1->GetLabel();
	m_Textlongitude1 = m_staticTextLongitudeCOMP1->GetLabel();
	m_Textaltitude1 = m_staticTextAltitudeCOMP1->GetLabel();
	m_TextSSID1 = m_staticTextSSIDCOMP1->GetLabel();
	m_TextMAC1 = m_staticTextMAC1->GetLabel();
	m_TextwifiPWR1 = m_staticTextPWRCOMP1->GetLabel();

	m_Textlatitude2 = m_staticTextLatitudeCOMP11->GetLabel();
	m_Textlongitude2 = m_staticTextLongitudeCOMP11->GetLabel();
	m_Textaltitude2 = m_staticTextAltitudeCOMP11->GetLabel();
	m_TextSSID2 = m_staticTextSSIDCOMP11->GetLabel();
	m_TextMAC2 = m_staticTextMAC2->GetLabel();
	m_TextwifiPWR2 = m_staticTextPWRCOMP11->GetLabel();

	m_Textlatitude3 = m_staticTextLatitudeCOMP12->GetLabel();
	m_Textlongitude3 = m_staticTextLongitudeCOMP12->GetLabel();
	m_Textaltitude3 = m_staticTextAltitudeCOMP12->GetLabel();
	m_TextSSID3 = m_staticTextSSIDCOMP12->GetLabel();
	m_TextMAC3 = m_staticTextMAC3->GetLabel();
	m_TextwifiPWR3 = m_staticTextPWRCOMP12->GetLabel();

	/*
	m_stlatitude4 = m_staticTextLatitudeCOMP13->GetLabel();
	m_stlongitude4 = m_staticTextLongitudeCOMP13->GetLabel();
	m_staltitude4 = m_staticTextAltitudeCOMP13->GetLabel();
	m_stSSID4 = m_staticTextSSIDCOMP13->GetLabel();
	m_TextMAC4 = m_staticTextMAC4->GetLabel();
	m_stwifiPWR4 = m_staticTextPWRCOMP13->GetLabel();
	*/
	//
	// End

	// Add network code
	wxIPV4address addr1;
	addr1.AnyAddress();
	addr1.Service(2201);

	sock1 = new wxDatagramSocket(addr1);
	sock1->SetEventHandler(*this, 41);
	sock1->SetNotify(wxSOCKET_INPUT_FLAG);
	sock1->Notify(true);

	bool errorCreatePort = false;
	if (sock1->IsOk() != true && errorCreatePort != true)
	{
		errorCreatePort = true;
		wxMessageBox(wxT("Не удалось открыть порт 2201.\nДальнейшая работа программы невозможна."), this->GetTitle(), wxOK | wxCENTRE | wxICON_ERROR);
		this->Close();
	}

	wxIPV4address addr2;
	addr2.AnyAddress();
	addr2.Service(2202);

	sock2 = new wxDatagramSocket(addr2);
	sock2->SetEventHandler(*this, 42);
	sock2->SetNotify(wxSOCKET_INPUT_FLAG);
	sock2->Notify(true);

	if (sock2->IsOk() != true && errorCreatePort != true)
	{
		errorCreatePort = true;
		wxMessageBox(wxT("Не удалось открыть порт 2202.\nДальнейшая работа программы невозможна."), this->GetTitle(), wxOK | wxCENTRE | wxICON_ERROR);
		this->Close();
	}

	wxIPV4address addr3;
	addr3.AnyAddress();
	addr3.Service(2203);

	sock3 = new wxDatagramSocket(addr3);
	sock3->SetEventHandler(*this, 43);
	sock3->SetNotify(wxSOCKET_INPUT_FLAG);
	sock3->Notify(true);

	if (sock3->IsOk() != true && errorCreatePort != true)
	{
		errorCreatePort = true;
		wxMessageBox(wxT("Не удалось открыть порт 2203.\nДальнейшая работа программы невозможна."), this->GetTitle(), wxOK | wxCENTRE | wxICON_ERROR);
		this->Close();
	}

	wxIPV4address addr4;
	addr4.AnyAddress();
	addr4.Service(2204);

	sock4 = new wxDatagramSocket(addr4);
	sock4->SetEventHandler(*this, 44);
	sock4->SetNotify(wxSOCKET_INPUT_FLAG);
	sock4->Notify(true);

	/*
	if (sock4->IsOk() != true && errorCreatePort != true)
	{
		errorCreatePort = true;
		wxMessageBox(wxT("Не удалось открыть порт 2204.\nДальнейшая работа программы невозможна."), this->GetTitle(), wxOK | wxCENTRE | wxICON_ERROR);
		this->Close();
	}
	*/

	m_updateLabel();

	// Connect Events
	this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MainFrame::MainFrameOnClose));
	this->Connect(wxEVT_HIBERNATE, wxActivateEventHandler(MainFrame::MainFrameOnHibernate));
	m_copy->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_copyOnButtonClick), NULL, this);
	m_buttonStop->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_buttonStopOnButtonClick), NULL, this);
	m_buttonStart->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_buttonStartOnButtonClick), NULL, this);
	m_menuGPS->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_menuItemSelectGPSOnMenuSelection), this, m_menuItemSelectGPS->GetId());
	m_menuGPS->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_menuItemInputCoordOnMenuSelection), this, m_menuItemInputCoord->GetId());
	m_menuWiFi->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_menuItemSelectWiFiAdapterOnMenuSelection), this, m_menuItemSelectWiFiAdapter->GetId());
	m_menuWiFi->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_menuItemSelectWiFiOnMenuSelection), this, m_menuItemSelectWiFi->GetId());
	m_menuNet->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_menuItemAddCompOnMenuSelection), this, m_menuItemAddComp->GetId());
	m_menuSettings->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_menuItemAverDataOnMenuSelection), this, m_menuItemAverData->GetId());
	m_help->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainFrame::m_aboutOnMenuSelection), this, m_about->GetId());
	//
}

MainFrame::~MainFrame()
{
	// Disconnect Events
	this->Disconnect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MainFrame::MainFrameOnClose));
	this->Disconnect(wxEVT_HIBERNATE, wxActivateEventHandler(MainFrame::MainFrameOnHibernate));
	m_copy->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_copyOnButtonClick), NULL, this);
	m_buttonStart->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_buttonStartOnButtonClick), NULL, this);
	m_buttonStop->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_buttonStopOnButtonClick), NULL, this);

	sock1->Destroy();
	sock2->Destroy();
	sock3->Destroy();
	sock4->Destroy();
}

void MainFrame::m_menuItemSelectGPSOnMenuSelection(wxCommandEvent& event)
{
	GPSSelect dialog(this);

	m_latitude = 0;
	m_longitude = 0;
	m_coordEntered = false;

	if (dialog.ShowModal() == wxID_APPLY)
	{
		m_comPort = dialog.GetCOMPort();
		m_comBaudrate = dialog.GetCOMBaudrate();
	}

	m_updateLabel();

	event.Skip();
}

void MainFrame::m_menuItemSelectWiFiAdapterOnMenuSelection(wxCommandEvent& event)
{
	SelectWiFiAdapterDialog dialog(this);

	if (dialog.ShowModal() == wxID_APPLY)
	{
		m_wifiAdapter = dialog.GetSelectAdapter();
	}

	m_updateLabel();

	event.Skip();
}


void MainFrame::m_menuItemSelectWiFiOnMenuSelection(wxCommandEvent& event)
{
	if (m_lockSelectWiFiDialog == true)
	{
		return;
	}

	m_lockSelectWiFiDialog = true;
	WiFiSelect dialog(std::wstring(m_wifiAdapter.begin(), m_wifiAdapter.end()), this);

	if (dialog.ShowModal() == wxID_APPLY)
	{
		m_wifiSSID = dialog.GetSSIDWiFi();
		m_wifiMAC = dialog.GetMACWiFi();
		m_wifiVisibleMAC = dialog.GetVisibleMACWiFi();
	}

	m_updateLabel();
	m_lockSelectWiFiDialog = false;

	event.Skip();
}

void MainFrame::m_menuItemAddCompOnMenuSelection(wxCommandEvent& event)
{
	AddComp dialog(this);

	if (dialog.ShowModal() == wxID_APPLY)
	{
		m_ipPortComp1 = dialog.GetIpAndPortComp1();
		m_ipPortComp2 = dialog.GetIpAndPortComp2();
		m_ipPortComp3 = dialog.GetIpAndPortComp3();
		//m_ipPortComp4 = dialog.GetIpAndPortComp4();
		m_ipPortServer = dialog.GetIpAndPortServer();
	}

	m_updateLabel();

	event.Skip();
}

void MainFrame::m_updateLabel()
{
	m_staticTextCOMPort->SetLabel(m_TextCOMPort + m_comPort);
	m_staticTextBaudrate->SetLabel(m_TextBaudrate + std::to_string(m_comBaudrate));

	if (m_useDekart == false)
	{
		m_staticTextLatitude->SetLabel(m_TextLatitude + std::to_string(m_latitude));
		m_staticTextLongitude->SetLabel(m_TextLongitude + std::to_string(m_longitude));
	}
	else
	{
		m_staticTextLatitude->SetLabel(m_TextX + std::to_string(m_latitude));
		m_staticTextLongitude->SetLabel(m_TextY + std::to_string(m_longitude));
	}
	m_staticTextAltitude->SetLabel(m_TextAltitude + std::to_string(m_altitude));

	m_staticTextWiFiAdapter->SetLabel(m_TextAdapter + m_wifiAdapter);
	m_staticTextSSID->SetLabel(m_TextSSID + m_wifiSSID);
	m_staticTextMAC->SetLabel(m_TextMAC + m_wifiVisibleMAC);
	m_staticTextPWR->SetLabel(m_TextPWR + std::to_string(m_wifiPWR));
	m_staticTextDistance->SetLabel(m_TextDistance + std::to_string(m_wifiDistance) + " м");

	m_staticTextComp1->SetLabel(m_TextComp1 + m_ipPortComp1 + m_ipStatus1);
	m_staticTextComp2->SetLabel(m_TextComp2 + m_ipPortComp2 + m_ipStatus2);
	m_staticTextComp3->SetLabel(m_TextComp3 + m_ipPortComp3 + m_ipStatus3);
	//m_staticTextComp4->SetLabel(m_TextComp4 + m_ipPortComp4 + m_ipStatus4);
	m_staticTextServer->SetLabel(m_TextServer + m_ipPortServer);

	if (m_useDekart == false)
	{
		m_staticTextLatitudeCOMP1->SetLabel(m_Textlatitude1 + std::to_string(m_latitude1));
		m_staticTextLongitudeCOMP1->SetLabel(m_Textlongitude1 + std::to_string(m_longitude1));
	}
	else
	{
		m_staticTextLatitudeCOMP1->SetLabel(m_TextX + std::to_string(m_latitude1));
		m_staticTextLongitudeCOMP1->SetLabel(m_TextY + std::to_string(m_longitude1));
	}
	m_staticTextAltitudeCOMP1->SetLabel(m_Textaltitude1 + std::to_string(m_altitude1));
	m_staticTextSSIDCOMP1->SetLabel(m_TextSSID1 + m_SSID1);
	m_staticTextMAC1->SetLabel(m_TextMAC1 + m_wifiVisibleMAC1);
	m_staticTextPWRCOMP1->SetLabel(m_TextwifiPWR1 + std::to_string(m_wifiPWR1));
	m_staticTextDistance1->SetLabel(m_TextDistance + std::to_string(m_wifiDistance1) + " м");

	if (m_useDekart == false)
	{
		m_staticTextLatitudeCOMP11->SetLabel(m_Textlatitude2 + std::to_string(m_latitude2));
		m_staticTextLongitudeCOMP11->SetLabel(m_Textlongitude2 + std::to_string(m_longitude2));
	}
	else
	{
		m_staticTextLatitudeCOMP11->SetLabel(m_TextX + std::to_string(m_latitude2));
		m_staticTextLongitudeCOMP11->SetLabel(m_TextY + std::to_string(m_longitude2));
	}
	m_staticTextAltitudeCOMP11->SetLabel(m_Textaltitude2 + std::to_string(m_altitude2));
	m_staticTextSSIDCOMP11->SetLabel(m_TextSSID2 + m_SSID2);
	m_staticTextMAC2->SetLabel(m_TextMAC2 + m_wifiVisibleMAC2);
	m_staticTextPWRCOMP11->SetLabel(m_TextwifiPWR2 + std::to_string(m_wifiPWR2));
	m_staticTextDistance2->SetLabel(m_TextDistance + std::to_string(m_wifiDistance2) + " м");

	if (m_useDekart == false)
	{
		m_staticTextLatitudeCOMP12->SetLabel(m_Textlatitude3 + std::to_string(m_latitude3));
		m_staticTextLongitudeCOMP12->SetLabel(m_Textlongitude3 + std::to_string(m_longitude3));
	}
	else
	{
		m_staticTextLatitudeCOMP12->SetLabel(m_TextX + std::to_string(m_latitude3));
		m_staticTextLongitudeCOMP12->SetLabel(m_TextY + std::to_string(m_longitude3));
	}
	m_staticTextAltitudeCOMP12->SetLabel(m_Textaltitude3 + std::to_string(m_altitude3));
	m_staticTextSSIDCOMP12->SetLabel(m_TextSSID3 + m_SSID3);
	m_staticTextMAC3->SetLabel(m_TextMAC3 + m_wifiVisibleMAC3);
	m_staticTextPWRCOMP12->SetLabel(m_TextwifiPWR3 + std::to_string(m_wifiPWR3));
	m_staticTextDistance3->SetLabel(m_TextDistance + std::to_string(m_wifiDistance3) + " м");

	/*
	if (m_useDekart == false)
	{
	m_staticTextLatitudeCOMP13->SetLabel(m_stlatitude4 + std::to_string(m_latitude4));
	m_staticTextLongitudeCOMP13->SetLabel(m_stlongitude4 + std::to_string(m_longitude4));
	}
	else
	{
	m_staticTextLatitudeCOMP13->SetLabel(m_TextX + std::to_string(m_latitude4));
	m_staticTextLongitudeCOMP13->SetLabel(m_TextY + std::to_string(m_longitude4));
	}
	m_staticTextAltitudeCOMP13->SetLabel(m_staltitude4 + std::to_string(m_altitude4));
	m_staticTextSSIDCOMP13->SetLabel(m_stSSID4 + m_SSID4);
	m_staticTextMAC4->SetLabel(m_TextMAC4 + m_wifiVisibleMAC4);
	m_staticTextPWRCOMP13->SetLabel(m_stwifiPWR4 + std::to_string(m_wifiPWR4));
	m_staticTextDistance4->SetLabel(m_TextDistance + std::to_string(m_wifiDistance4) + " м");
	*/

	if (m_useDekart == false)
	{
		m_staticTextLatitudeOut->SetLabel(m_TextLatitudeOut + std::to_string(m_outLatitude));
		m_staticTextLongitudeOut->SetLabel(m_TextLongitudeOut + std::to_string(m_outLongitude));
		m_staticTextAzimuth->Show(true);
		m_hyperlinkShowMap->Show(true);
	}
	else
	{
		m_staticTextLatitudeOut->SetLabel(m_TextX + std::to_string(m_outLatitude));
		m_staticTextLongitudeOut->SetLabel(m_TextY + std::to_string(m_outLongitude));
		m_staticTextAzimuth->Show(false);
		m_hyperlinkShowMap->Show(false);
	}
	//m_staticTextAltitudeOut->SetLabel(m_TextAltitudeOut + std::to_string(m_outAltitude));
	m_staticTextAzimuth->SetLabel(m_TextAzimuth + std::to_string(m_outAzimuth));
	m_staticTextCoordDistance->SetLabel(m_TextDistance + std::to_string(m_outCoordDistance) + " м");
	m_staticTextAllStatus->SetLabel(m_TextAllStatus + m_allStatus);

	bSizer11->RepositionChildren(bSizer11->CalcMin());

	Refresh();
	Update();
}

void MainFrame::m_buttonStartOnButtonClick(wxCommandEvent& event)
{
	NetworkClearAllData();

	COMPort gps;

	ClearAverageGPSCoord();
	ClearAverageBuffGPSCoord();

	ClearMinimumPWR();
	ClearAveragePWR();
	ClearAverageBuffPWR();

	ClearAverageFindCoord();
	ClearAverageBuffFindCoord();

	if (m_coordEntered == false)
	{
		try
		{
			gps.Create(m_comPort, m_comBaudrate);
		}
		catch (...)
		{
			wxMessageBox(wxT("Не выбран GPS адаптер."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);
			return;
		}
	}

	Wlan scan;

	try
	{
		scan.Create(std::wstring(m_wifiAdapter.begin(), m_wifiAdapter.end()));
	}
	catch (...)
	{
		wxMessageBox(wxT("Не выбран Wi-Fi адаптер."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);
		return;
	}

	if (m_wifiMAC == "")
	{
		wxMessageBox(wxT("Не выбрана точка доступа Wi-Fi."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);

		return;
	}

	Wlan::WiFiPoint PWRinfo("", "", "", 0, 0);

	m_buttonStart->Enable(false);
	m_buttonStop->Enable(true);

	m_menuItemSelectGPS->Enable(false);
	m_menuItemSelectWiFiAdapter->Enable(false);
	m_menuItemSelectWiFi->Enable(false);

	m_runThread = true;

	for (;;)
	{
		m_updateLabel();
		wxApp::GetInstance()->Yield();

		if (m_stopThread == true)
		{
			m_buttonStop->Enable(false);
			m_buttonStart->Enable(true);

			m_menuItemSelectGPS->Enable(true);
			m_menuItemSelectWiFiAdapter->Enable(true);
			m_menuItemSelectWiFi->Enable(true);

			m_stopThread = false;
			m_runThread = false;

			if (m_exit == true)
			{
				this->Close();
			}

			return;
		}

		// считывание и парсинг строк
		if (m_coordEntered == false)
		{
			try
			{
				ParseNMEA coord(gps.ReadLine());

				m_altitude = coord.GetGPSAltitude();

				if ((countGPSCoord > AverageCount) && (AverageCount != 0))
				{
					ClearAverageGPSCoord();
				}

				if (AverageModeGPS == 0)
				{
					m_latitude = coord.GetGPSLatitude();
					m_longitude = coord.GetGPSLongitude();
				}
				else if (AverageModeGPS == 1)
				{
					countGPSCoord = countGPSCoord + 1;
					m_latitude = FindAverageGPSCoord1(coord.GetGPSLatitude(), countGPSCoord);
					m_longitude = FindAverageGPSCoord2(coord.GetGPSLongitude(), countGPSCoord);
				}
				else if (AverageModeGPS == 2)
				{
					m_latitude = FindAverageBuffGPSCoord1(coord.GetGPSLatitude());
					m_longitude = FindAverageBuffGPSCoord2(coord.GetGPSLongitude());
				}
				else
				{
					m_latitude = coord.GetGPSLatitude();
					m_longitude = coord.GetGPSLongitude();

					//throw std::runtime_error("GPS settings error.");
				}

			}
			catch (...)
			{
				continue;
			}
		}
		m_coordUsed = true;

		m_updateLabel();
		wxApp::GetInstance()->Yield();

		if (m_stopThread == true)
		{
			m_buttonStop->Enable(false);
			m_buttonStart->Enable(true);

			m_menuItemSelectGPS->Enable(true);
			m_menuItemSelectWiFiAdapter->Enable(true);
			m_menuItemSelectWiFi->Enable(true);

			m_stopThread = false;
			m_runThread = false;

			if (m_exit == true)
			{
				this->Close();
			}

			return;
		}

		try
		{
			PWRinfo = scan.GetInfoWiFi(m_wifiMAC);
		}
		catch (...)
		{
			m_allStatus = "Не найден сигнал Wi-Fi";
			continue;
		}

		m_updateLabel();
		wxApp::GetInstance()->Yield();

		if (m_stopThread == true)
		{
			m_buttonStop->Enable(false);
			m_buttonStart->Enable(true);

			m_menuItemSelectGPS->Enable(true);
			m_menuItemSelectWiFiAdapter->Enable(true);
			m_menuItemSelectWiFi->Enable(true);

			m_stopThread = false;
			m_runThread = false;

			if (m_exit == true)
			{
				this->Close();
			}

			return;
		}

		if ((countPWR > AverageCount) && (AverageCount != 0))
		{
			ClearMinimumPWR();
			ClearAveragePWR();
		}

		if (AverageModePWR == 0)
		{
			m_wifiPWR = PWRinfo.PWR;
		}
		else if (AverageModePWR == 1)
		{
			countPWR = countPWR + 1;
			m_wifiPWR = FindMinimumPWR(PWRinfo.PWR, m_wifiPWR);
		}
		else if (AverageModePWR == 2)
		{
			countPWR = countPWR + 1;
			m_wifiPWR = FindAveragePWR(PWRinfo.PWR, countPWR);
		}
		else if (AverageModePWR == 3)
		{
			m_wifiPWR = FindAverageBuffPWR(PWRinfo.PWR);
		}
		else
		{
			m_wifiPWR = PWRinfo.PWR;

			//throw std::runtime_error("PWR settings error.");
		}

		m_wifiDistance = Trilateration::CalculateDistance(m_wifiPWR, PWRinfo.freq);

		// Рассылка данных
		if (m_ipPortServer != "")
			SendData(m_ipPortServer, "2199", m_latitude, m_longitude, m_altitude, m_wifiPWR, PWRinfo.freq, m_wifiVisibleMAC, m_wifiSSID);

		if (m_ipPortComp1 != "")
			SendData(m_ipPortComp1, "2201", m_latitude, m_longitude, m_altitude, m_wifiPWR, PWRinfo.freq, m_wifiVisibleMAC, m_wifiSSID);

		if (m_ipPortComp2 != "")
			SendData(m_ipPortComp2, "2202", m_latitude, m_longitude, m_altitude, m_wifiPWR, PWRinfo.freq, m_wifiVisibleMAC, m_wifiSSID);

		if (m_ipPortComp3 != "")
			SendData(m_ipPortComp3, "2203", m_latitude, m_longitude, m_altitude, m_wifiPWR, PWRinfo.freq, m_wifiVisibleMAC, m_wifiSSID);
		/*
		if (m_ipPortComp4 != "")
			SendData(m_ipPortComp4, "2204", m_latitude, m_longitude, m_altitude, m_wifiPWR, PWRinfo.freq, m_wifiVisibleMAC, m_wifiSSID);
		*/

		// Расчет
		Trilateration::Position outCoord(0, 0);

		try
		{
			if (m_useDekart == false)
			{
				outCoord = Trilateration::FindPosition(m_coordUsed, m_latitude, m_longitude, m_wifiPWR, m_coordUsed1, m_latitude1, m_longitude1, m_wifiPWR1, m_coordUsed2, m_latitude2, m_longitude2, m_wifiPWR2, m_coordUsed3, m_latitude3, m_longitude3, m_wifiPWR3, m_coordUsed4, m_latitude4 = 0, m_longitude4 = 0, m_wifiPWR4 = 0, PWRinfo.freq);

				if ((countFindCoord > AverageCount) && (AverageCount != 0))
				{
					ClearAverageFindCoord();
				}

				if (AverageModeFindCoord == 0)
				{

				}
				else if (AverageModeFindCoord == 1)
				{
					countFindCoord = countFindCoord + 1;
					outCoord.latitude = FindAverageFindCoord1(outCoord.latitude, countFindCoord);
					outCoord.longitude = FindAverageFindCoord2(outCoord.longitude, countFindCoord);
				}
				else if (AverageModeFindCoord == 2)
				{
					outCoord.latitude = FindAverageBuffFindCoord1(outCoord.latitude);
					outCoord.longitude = FindAverageBuffFindCoord2(outCoord.longitude);
				}
				else
				{
					//throw std::runtime_error("FindCoord settings error.");
				}

				Trilateration::FindCoordAzimuth(m_latitude, m_longitude, outCoord.latitude, outCoord.longitude, m_outAzimuth);
				Trilateration::FindCoordDistance(m_latitude, m_longitude, outCoord.latitude, outCoord.longitude, m_outCoordDistance);
			}
			else
			{
				outCoord = Trilateration::FindDekartPosition(m_coordUsed, m_latitude, m_longitude, m_wifiPWR, m_coordUsed1, m_latitude1, m_longitude1, m_wifiPWR1, m_coordUsed2, m_latitude2, m_longitude2, m_wifiPWR2, m_coordUsed3, m_latitude3, m_longitude3, m_wifiPWR3, m_coordUsed4, m_latitude4 = 0, m_longitude4 = 0, m_wifiPWR4 = 0, PWRinfo.freq);

				if ((countFindCoord > AverageCount) && (AverageCount != 0))
				{
					ClearAverageFindCoord();
				}

				if (AverageModeFindCoord == 0)
				{

				}
				else if (AverageModeFindCoord == 1)
				{
					countFindCoord = countFindCoord + 1;
					outCoord.latitude = FindAverageFindCoord1(outCoord.latitude, countFindCoord);
					outCoord.longitude = FindAverageFindCoord2(outCoord.longitude, countFindCoord);
				}
				else if (AverageModeFindCoord == 2)
				{
					outCoord.latitude = FindAverageBuffFindCoord1(outCoord.latitude);
					outCoord.longitude = FindAverageBuffFindCoord2(outCoord.longitude);
				}
				else
				{
					//throw std::runtime_error("FindCoord settings error.");
				}

				Trilateration::FindDekartDistance(m_latitude, m_longitude, outCoord.latitude, outCoord.longitude, m_outCoordDistance);
			}
		}
		catch (...)
		{
			m_allStatus = "Не найдено местоположение Wi-Fi";
			continue;
		}

		m_outLatitude = outCoord.latitude;
		m_outLongitude = outCoord.longitude;

		m_allStatus = "Найдено местоположение Wi-Fi";

		m_updateLabel();
		wxApp::GetInstance()->Yield();

		m_copy->Enable(true);

		// Доступ к карте
		if (m_useDekart == false)
		{
			m_hyperlinkShowMap->SetURL("https://www.openstreetmap.org/directions?engine=fossgis_osrm_foot&route=" + std::to_string(m_latitude) + "%2C" + std::to_string(m_longitude) + "%3B" + std::to_string(m_outLatitude) + "%2C" + std::to_string(m_outLongitude) + "#map=19/" + std::to_string(m_outLatitude) + "/" + std::to_string(m_outLongitude));
			m_hyperlinkShowMap->Enable(true);
		}
		else
		{
			m_hyperlinkShowMap->Enable(false);
		}
	}

	event.Skip();
}

void MainFrame::m_buttonStopOnButtonClick(wxCommandEvent& event)
{
	m_stopThread = true;

	event.Skip();
}

void MainFrame::m_copyOnButtonClick(wxCommandEvent& event)
{
	if (wxTheClipboard->Open() == true)
	{
		wxTheClipboard->SetData(new wxTextDataObject(std::to_string(m_outLatitude) + " " + std::to_string(m_outLongitude)));
		wxTheClipboard->Close();
	}

	event.Skip();
}

void MainFrame::m_aboutOnMenuSelection(wxCommandEvent& event)
{
	wxMessageBox("Текущая версия программы: " + m_version + "\n" + "Разработчики: Р.Р. Галимов, Д.С. Васильев, Д.С. Гусельников." + "\n" + "\n" + "В данной программе используются следующие библиотеки: " + "\n" + "wxWidgets, Serial Communication, GeographicLib, Minmea." + "\n" + "\n" + "Email: dmitrygdpwf@mail.ru", this->GetTitle(), wxOK | wxICON_INFORMATION);

	event.Skip();
}

void MainFrame::m_menuItemInputCoordOnMenuSelection(wxCommandEvent& event)
{
	InputCoordinatesDialog dialog(this);

	m_comPort = "";
	m_comBaudrate = 0;

	if (dialog.ShowModal() == wxID_APPLY)
	{
		m_latitude = dialog.GetLatitude0();
		m_longitude = dialog.GetLongitude0();
		m_altitude = 0;

		m_coordEntered = true;
		m_useDekart = dialog.GetUseDekart();

		ClearAverageGPSCoord();
		ClearAverageBuffGPSCoord();

		ClearMinimumPWR();
		ClearAveragePWR();
		ClearAverageBuffPWR();

		ClearAverageFindCoord();
		ClearAverageBuffFindCoord();
	}

	m_updateLabel();

	event.Skip();
}

void MainFrame::MainFrameOnClose(wxCloseEvent& event)
{
	if (m_runThread == false)
	{
		event.Skip();
	}

	m_stopThread = true;
	m_exit = true;    // костыль, нужен для "безопасной" остановки потока с вычисл.
}

void MainFrame::m_menuItemAverDataOnMenuSelection(wxCommandEvent& event)
{
	AverageData dialog(this);

	if (dialog.ShowModal() == wxID_APPLY)
	{
		AverageModeGPS = dialog.GetAverageModeGPS();
		AverageModePWR = dialog.GetAverageModePWR();
		AverageModeFindCoord = dialog.GetAverageModeFindCoord();

		AverageCount = dialog.GetCountAverage();
		AverageSizeBuff = dialog.GetSizeBuffAverage();

		ClearAverageGPSCoord();
		ClearAverageBuffGPSCoord();

		ClearMinimumPWR();
		ClearAveragePWR();
		ClearAverageBuffPWR();

		ClearAverageFindCoord();
		ClearAverageBuffFindCoord();
	}

	m_updateLabel();

	event.Skip();
}

double MainFrame::FindMinimumPWR(double newNumber, double previousNumber)
{
	if (newNumber > previousNumber)
	{
		return newNumber;
	}
	else
	{
		return previousNumber;
	}
}

void MainFrame::ClearMinimumPWR()
{
	if (AverageModePWR == 1)
	{
		minimumPWR = std::numeric_limits<double>::max();
		m_wifiPWR = -999999;
		countPWR = 0;
	}
}

double MainFrame::FindAveragePWR(double newNumber, size_t count)
{
	double temp = 0;
	averagePWR.push_back(newNumber);

	for (double i : averagePWR)
	{
		temp = temp + i;
	}

	return (double)(temp / count);
}

void MainFrame::ClearAveragePWR()
{
	if (AverageModePWR == 2)
	{
		averagePWR.clear();
		countPWR = 0;
	}
}

double MainFrame::FindAverageBuffPWR(double newNumber)
{
	if (AverageSizeBuff <= 0) AverageSizeBuff = 1;
	double temp = 0;

	if (averageBuffPWR.size() < AverageSizeBuff)
	{
		averageBuffPWR.push_back(newNumber);
	}
	else
	{
		averageBuffPWR.pop_front();
		averageBuffPWR.push_back(newNumber);
	}

	for (double i : averageBuffPWR)
	{
		temp = temp + i;
	}

	return (double)(temp / averageBuffPWR.size());
}

void MainFrame::ClearAverageBuffPWR()
{
	if (AverageModePWR == 3)
	{
		averageBuffPWR.clear();
	}
}

double MainFrame::FindAverageGPSCoord1(double newNumber, size_t count)
{
	double temp = 0;
	averageGPSCoord1.push_back(newNumber);

	for (double i : averageGPSCoord1)
	{
		temp = temp + i;
	}

	return (double)(temp / count);
}

double MainFrame::FindAverageGPSCoord2(double newNumber, size_t count)
{
	double temp = 0;
	averageGPSCoord2.push_back(newNumber);

	for (double i : averageGPSCoord2)
	{
		temp = temp + i;
	}

	return (double)(temp / count);
}

void MainFrame::ClearAverageGPSCoord()
{
	if (AverageModeGPS == 1)
	{
		averageGPSCoord1.clear();
		averageGPSCoord2.clear();
		countGPSCoord = 0;
	}
}

double MainFrame::FindAverageBuffGPSCoord1(double newNumber)
{
	if (AverageSizeBuff <= 0) AverageSizeBuff = 1;
	double temp = 0;

	if (averageBuffGPSCoord1.size() < AverageSizeBuff)
	{
		averageBuffGPSCoord1.push_back(newNumber);
	}
	else
	{
		averageBuffGPSCoord1.pop_front();
		averageBuffGPSCoord1.push_back(newNumber);
	}

	for (double i : averageBuffGPSCoord1)
	{
		temp = temp + i;
	}

	return (double)(temp / averageBuffGPSCoord1.size());
}

double MainFrame::FindAverageBuffGPSCoord2(double newNumber)
{
	if (AverageSizeBuff <= 0) AverageSizeBuff = 1;
	double temp = 0;

	if (averageBuffGPSCoord2.size() < AverageSizeBuff)
	{
		averageBuffGPSCoord2.push_back(newNumber);
	}
	else
	{
		averageBuffGPSCoord2.pop_front();
		averageBuffGPSCoord2.push_back(newNumber);
	}

	for (double i : averageBuffGPSCoord2)
	{
		temp = temp + i;
	}

	return (double)(temp / averageBuffGPSCoord2.size());
}

void MainFrame::ClearAverageBuffGPSCoord()
{
	if (AverageModeGPS == 2)
	{
		averageBuffGPSCoord1.clear();
		averageBuffGPSCoord2.clear();
	}
}

double MainFrame::FindAverageFindCoord1(double newNumber, size_t count)
{
	double temp = 0;
	averageFindCoord1.push_back(newNumber);

	for (double i : averageFindCoord1)
	{
		temp = temp + i;
	}

	return (double)(temp / count);
}

double MainFrame::FindAverageFindCoord2(double newNumber, size_t count)
{
	double temp = 0;
	averageFindCoord2.push_back(newNumber);

	for (double i : averageFindCoord2)
	{
		temp = temp + i;
	}

	return (double)(temp / count);
}

void MainFrame::ClearAverageFindCoord()
{
	if (AverageModeFindCoord == 1)
	{
		averageFindCoord1.clear();
		averageFindCoord2.clear();
		countFindCoord = 0;
	}
}

double MainFrame::FindAverageBuffFindCoord1(double newNumber)
{
	if (AverageSizeBuff <= 0) AverageSizeBuff = 1;
	double temp = 0;

	if (averageBuffFindCoord1.size() < AverageSizeBuff)
	{
		averageBuffFindCoord1.push_back(newNumber);
	}
	else
	{
		averageBuffFindCoord1.pop_front();
		averageBuffFindCoord1.push_back(newNumber);
	}

	for (double i : averageBuffFindCoord1)
	{
		temp = temp + i;
	}

	return (double)(temp / averageBuffFindCoord1.size());
}

double MainFrame::FindAverageBuffFindCoord2(double newNumber)
{
	if (AverageSizeBuff <= 0) AverageSizeBuff = 1;
	double temp = 0;

	if (averageBuffFindCoord2.size() < AverageSizeBuff)
	{
		averageBuffFindCoord2.push_back(newNumber);
	}
	else
	{
		averageBuffFindCoord2.pop_front();
		averageBuffFindCoord2.push_back(newNumber);
	}

	for (double i : averageBuffFindCoord2)
	{
		temp = temp + i;
	}

	return (double)(temp / averageBuffFindCoord2.size());
}

void MainFrame::ClearAverageBuffFindCoord()
{
	if (AverageModeFindCoord == 2)
	{
		averageBuffFindCoord1.clear();
		averageBuffFindCoord2.clear();
	}
}

void MainFrame::NetworkClearAllData()
{
	m_latitude1 = 0;
	m_longitude1 = 0;
	m_altitude1 = 0;
	m_coordUsed1 = false;
	m_SSID1 = "";
	m_wifiVisibleMAC1 = "";
	m_wifiPWR1 = 0;
	m_wifiDistance1 = 0;

	m_latitude2 = 0;
	m_longitude2 = 0;
	m_altitude2 = 0;
	m_coordUsed2 = false;
	m_SSID2 = "";
	m_wifiVisibleMAC2 = "";
	m_wifiPWR2 = 0;
	m_wifiDistance2 = 0;

	m_latitude3 = 0;
	m_longitude3 = 0;
	m_altitude3 = 0;
	m_coordUsed3 = false;
	m_SSID3 = "";
	m_wifiVisibleMAC3 = "";
	m_wifiPWR3 = 0;
	m_wifiDistance3 = 0;

	m_latitude4 = 0;
	m_longitude4 = 0;
	m_altitude4 = 0;
	m_coordUsed4 = false;
	m_SSID4 = "";
	m_wifiVisibleMAC4 = "";
	m_wifiPWR4 = 0;
	m_wifiDistance4 = 0;

	m_ipStatus1 = "";
	m_ipStatus2 = "";
	m_ipStatus3 = "";
	m_ipStatus4 = "";

	m_allStatus = "";
}