﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class InputCoordinatesDialog
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class InputCoordinatesDialog : public wxDialog
	{
	private:

	protected:
		wxStaticText* m_staticText1;
		wxStaticText* m_staticText2;
		wxTextCtrl* m_inputLatitude0;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_inputLongitude0;
		wxCheckBox* m_checkBoxDekart;
		wxButton* m_AddCoord;

		// Virtual event handlers, overide them in your derived class
		virtual void m_checkBoxDekartOnCheckBox(wxCommandEvent& event);
		virtual void m_AddCoordOnButtonClick(wxCommandEvent& event);


	public:

		InputCoordinatesDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Ввести координаты"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(700, 229), long style = wxDEFAULT_DIALOG_STYLE);
		~InputCoordinatesDialog();

		double GetLatitude0()
		{
			wxString temp = m_inputLatitude0->GetLineText(0);
			if (temp == "") return 0;

			double out = 0;
			try
			{
				out = std::stod((std::string)temp);
			}
			catch (...)
			{
			}

			return out;
		}

		double GetLongitude0()
		{
			wxString temp = m_inputLongitude0->GetLineText(0);
			if (temp == "") return 0;

			double out = 0;
			try
			{
				out = std::stod((std::string)temp);
			}
			catch (...)
			{
			}

			return out;
		}

		bool GetUseDekart()
		{
			return m_checkBoxDekart->IsChecked();
		}

	};
}