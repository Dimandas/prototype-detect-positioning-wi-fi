﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/choice.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/dialog.h>

#include "../../gps/com.h"

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class GPSSelect
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class GPSSelect : public wxDialog
	{
	private:

	protected:
		wxStaticText* m_staticText2;
		wxChoice* m_ListGPS;
		wxButton* m_UpdateList;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_GPSPort;
		wxStaticText* m_staticText4;
		wxTextCtrl* m_baudrate;
		wxButton* m_SelectGPS;

		// Add user code
		std::vector<serial::PortInfo> m_GPSList;
		void m_GPSListUpdate();
		//

		// Virtual event handlers, overide them in your derived class
		virtual void m_ListGPSOnChoice(wxCommandEvent& event);
		virtual void m_UpdateListOnButtonClick(wxCommandEvent& event);
		virtual void m_SelectGPSOnButtonClick(wxCommandEvent& event);


	public:

		GPSSelect(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Выбрать GPS адаптер"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(700, 348), long style = wxDEFAULT_DIALOG_STYLE);
		~GPSSelect();

		// Add user code
		inline std::string GetCOMPort()
		{
			return (std::string)m_GPSPort->GetLineText(0);
		}
		inline int GetCOMBaudrate()
		{
			wxString temp = m_baudrate->GetLineText(0);
			if (temp == "") return 0;

			int out = 0;
			try
			{
				out = std::stoi((std::string)temp);
			}
			catch (...)
			{
			}

			return out;
		}
		// End

	};
}
