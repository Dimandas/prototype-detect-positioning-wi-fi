﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wifiadapter.h"
#include <wx/msgdlg.h>

#include "../../wifi/wlan.h"

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

SelectWiFiAdapterDialog::SelectWiFiAdapterDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText20 = new wxStaticText(this, wxID_ANY, wxT("Выбрать из списка:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText20->Wrap(-1);
	bSizer10->Add(m_staticText20, 0, wxALL, 5);


	bSizer10->Add(0, 0, 1, wxEXPAND, 5);

	wxArrayString m_WiFiAdapterListChoices;
	m_WiFiAdapterList = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(400, -1), m_WiFiAdapterListChoices, 0);
	m_WiFiAdapterList->SetSelection(0);
	bSizer10->Add(m_WiFiAdapterList, 0, wxALL, 5);

	m_updateAdapterList = new wxButton(this, wxID_ANY, wxT("Обновить"), wxDefaultPosition, wxSize(-1, 30), 0);
	bSizer10->Add(m_updateAdapterList, 0, wxALL, 5);


	bSizer9->Add(bSizer10, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer(wxVERTICAL);


	bSizer11->Add(0, 0, 1, wxEXPAND, 5);

	m_SelectAdapter = new wxButton(this, wxID_ANY, wxT("Выбрать"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer11->Add(m_SelectAdapter, 0, wxALL | wxEXPAND, 5);


	bSizer9->Add(bSizer11, 1, wxEXPAND, 5);


	this->SetSizer(bSizer9);
	this->Layout();

	this->Centre(wxBOTH);

	// Add user code
	m_WiFiAdapterListUpdate();
	// End

	// Connect Events
	m_updateAdapterList->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectWiFiAdapterDialog::m_updateAdapterListOnButtonClick), NULL, this);
	m_SelectAdapter->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectWiFiAdapterDialog::m_SelectAdapterOnButtonClick), NULL, this);
}

SelectWiFiAdapterDialog::~SelectWiFiAdapterDialog()
{
	// Disconnect Events
	m_updateAdapterList->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectWiFiAdapterDialog::m_updateAdapterListOnButtonClick), NULL, this);
	m_SelectAdapter->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectWiFiAdapterDialog::m_SelectAdapterOnButtonClick), NULL, this);
}

void SelectWiFiAdapterDialog::m_WiFiAdapterListUpdate()
{
	std::vector <std::wstring> AdapterList;

	try
	{
		AdapterList = Wlan::GetListAdapters();
		if (AdapterList.size() == 0)
		{
			throw std::runtime_error("Wi-Fi adapter is not found");
		}
	}
	catch (...)
	{
		wxMessageBox("Адаптеры Wi-Fi не найдены.", this->GetTitle());
	}

	m_WiFiAdapterList->Clear();

	for (size_t i = 0; i < AdapterList.size(); i++)
	{
		m_WiFiAdapterList->Append(AdapterList[i]);
	}
}

void SelectWiFiAdapterDialog::m_updateAdapterListOnButtonClick(wxCommandEvent& event)
{
	m_WiFiAdapterListUpdate();

	event.Skip();
}

void SelectWiFiAdapterDialog::m_SelectAdapterOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}