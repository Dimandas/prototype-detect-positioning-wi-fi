/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "averagedata.h"

#include <wx/valtext.h>

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

AverageData::AverageData(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText6 = new wxStaticText(this, wxID_ANY, wxT("���������� � GPS ��������"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText6->Wrap(-1);
	bSizer13->Add(m_staticText6, 0, wxALL, 5);


	bSizer13->Add(0, 0, 1, wxEXPAND, 5);

	wxString m_choiceGPSChoices[] = { wxT("�� ��������� ������"), wxT("������� ��������������"), wxT("������� �������������� � �������") };
	int m_choiceGPSNChoices = sizeof(m_choiceGPSChoices) / sizeof(wxString);
	m_choiceGPS = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choiceGPSNChoices, m_choiceGPSChoices, 0);
	m_choiceGPS->SetSelection(0);
	bSizer13->Add(m_choiceGPS, 0, wxALL, 5);


	bSizer11->Add(bSizer13, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText7 = new wxStaticText(this, wxID_ANY, wxT("PWR � Wi-Fi ��������"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText7->Wrap(-1);
	bSizer14->Add(m_staticText7, 0, wxALL, 5);


	bSizer14->Add(0, 0, 1, wxEXPAND, 5);

	wxString m_choicePWRChoices[] = { wxT("�� ��������� ������"), wxT("�� ������������"), wxT("������� ��������������"), wxT("������� �������������� � �������") };
	int m_choicePWRNChoices = sizeof(m_choicePWRChoices) / sizeof(wxString);
	m_choicePWR = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choicePWRNChoices, m_choicePWRChoices, 0);
	m_choicePWR->SetSelection(0);
	bSizer14->Add(m_choicePWR, 0, wxALL, 5);


	bSizer11->Add(bSizer14, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText8 = new wxStaticText(this, wxID_ANY, wxT("��������� ����������"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText8->Wrap(-1);
	bSizer15->Add(m_staticText8, 0, wxALL, 5);


	bSizer15->Add(0, 0, 1, wxEXPAND, 5);

	wxString m_choiceFindCoordChoices[] = { wxT("�� ��������� ������"), wxT("������� ��������������"), wxT("������� �������������� � �������") };
	int m_choiceFindCoordNChoices = sizeof(m_choiceFindCoordChoices) / sizeof(wxString);
	m_choiceFindCoord = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choiceFindCoordNChoices, m_choiceFindCoordChoices, 0);
	m_choiceFindCoord->SetSelection(0);
	bSizer15->Add(m_choiceFindCoord, 0, wxALL, 5);


	bSizer11->Add(bSizer15, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer(wxHORIZONTAL);


	bSizer17->Add(0, 0, 1, wxEXPAND, 5);


	bSizer11->Add(bSizer17, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer(wxHORIZONTAL);

	// add user code
	wxTextValidator valid(wxFILTER_INCLUDE_CHAR_LIST, nullptr);
	valid.SetCharIncludes("0123456789");
	// end

	m_staticText9 = new wxStaticText(this, wxID_ANY, wxT("���������� ���������� ������ �����:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText9->Wrap(-1);
	bSizer16->Add(m_staticText9, 0, wxALL, 5);


	bSizer16->Add(0, 0, 1, wxEXPAND, 5);

	m_textCtrlCountAverage = new wxTextCtrl(this, wxID_ANY, wxT("15"), wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_textCtrlCountAverage->HasFlag(wxTE_MULTILINE))
	{
		m_textCtrlCountAverage->SetMaxLength(5);
	}
#else
	m_textCtrlCountAverage->SetMaxLength(5);
#endif
	bSizer16->Add(m_textCtrlCountAverage, 0, wxALL, 5);


	bSizer11->Add(bSizer16, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText10 = new wxStaticText(this, wxID_ANY, wxT("������ ������:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText10->Wrap(-1);
	bSizer18->Add(m_staticText10, 0, wxALL, 5);


	bSizer18->Add(0, 0, 1, wxEXPAND, 5);

	m_textCtrlSizeBuff = new wxTextCtrl(this, wxID_ANY, wxT("15"), wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_textCtrlSizeBuff->HasFlag(wxTE_MULTILINE))
	{
		m_textCtrlSizeBuff->SetMaxLength(5);
	}
#else
	m_textCtrlSizeBuff->SetMaxLength(5);
#endif
	bSizer18->Add(m_textCtrlSizeBuff, 0, wxALL, 5);


	bSizer11->Add(bSizer18, 1, wxEXPAND, 5);


	bSizer10->Add(bSizer11, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer(wxVERTICAL);


	bSizer12->Add(0, 0, 1, wxEXPAND, 5);

	m_select = new wxButton(this, wxID_ANY, wxT("�������"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer12->Add(m_select, 0, wxALL | wxEXPAND, 5);


	bSizer10->Add(bSizer12, 1, wxEXPAND, 5);


	this->SetSizer(bSizer10);
	this->Layout();

	this->Centre(wxBOTH);

	// Connect Events
	m_select->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AverageData::m_selectOnButtonClick), NULL, this);
}

AverageData::~AverageData()
{
	// Disconnect Events
	m_select->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AverageData::m_selectOnButtonClick), NULL, this);
}

void AverageData::m_selectOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}