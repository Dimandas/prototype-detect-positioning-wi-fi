﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gps.h"

#include <wx/msgdlg.h>
#include <wx/valtext.h>

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

GPSSelect::GPSSelect(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText2 = new wxStaticText(this, wxID_ANY, wxT("Выбрать из списка:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText2->Wrap(-1);
	bSizer5->Add(m_staticText2, 0, wxALL, 5);


	bSizer5->Add(0, 0, 1, wxEXPAND, 5);

	wxArrayString m_ListGPSChoices;
	m_ListGPS = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(400, -1), m_ListGPSChoices, 0);
	m_ListGPS->SetSelection(0);
	bSizer5->Add(m_ListGPS, 0, wxALL, 5);

	m_UpdateList = new wxButton(this, wxID_ANY, wxT("Обновить"), wxDefaultPosition, wxSize(-1, 30), 0);
	bSizer5->Add(m_UpdateList, 0, wxALL, 5);


	bSizer2->Add(bSizer5, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText3 = new wxStaticText(this, wxID_ANY, wxT("Порт:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText3->Wrap(-1);
	bSizer6->Add(m_staticText3, 0, wxALL, 5);

	m_GPSPort = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
#ifdef __WXGTK__
	if (!m_GPSPort->HasFlag(wxTE_MULTILINE))
	{
		m_GPSPort->SetMaxLength(28);
	}
#else
	m_GPSPort->SetMaxLength(28);
#endif
	bSizer6->Add(m_GPSPort, 0, wxALL, 5);


	bSizer6->Add(0, 0, 1, wxEXPAND, 5);

	m_staticText4 = new wxStaticText(this, wxID_ANY, wxT("Скорость передачи:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText4->Wrap(-1);
	bSizer6->Add(m_staticText4, 0, wxALL, 5);

	m_baudrate = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxTextValidator(wxFILTER_DIGITS, nullptr));
#ifdef __WXGTK__
	if (!m_baudrate->HasFlag(wxTE_MULTILINE))
	{
		m_baudrate->SetMaxLength(10);
	}
#else
	m_baudrate->SetMaxLength(10);
#endif
	bSizer6->Add(m_baudrate, 0, wxALL, 5);


	bSizer2->Add(bSizer6, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer2, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);


	bSizer3->Add(0, 0, 1, wxEXPAND, 5);

	m_SelectGPS = new wxButton(this, wxID_ANY, wxT("Выбрать"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer3->Add(m_SelectGPS, 0, wxALL | wxEXPAND, 5);


	bSizer1->Add(bSizer3, 1, wxEXPAND, 5);


	this->SetSizer(bSizer1);
	this->Layout();

	this->Centre(wxBOTH);

	// Add user code
	m_GPSListUpdate();
	// End

	// Connect Events
	m_ListGPS->Connect(wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler(GPSSelect::m_ListGPSOnChoice), NULL, this);
	m_UpdateList->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(GPSSelect::m_UpdateListOnButtonClick), NULL, this);
	m_SelectGPS->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(GPSSelect::m_SelectGPSOnButtonClick), NULL, this);
}

GPSSelect::~GPSSelect()
{
	// Disconnect Events
	m_ListGPS->Disconnect(wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler(GPSSelect::m_ListGPSOnChoice), NULL, this);
	m_UpdateList->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(GPSSelect::m_UpdateListOnButtonClick), NULL, this);
	m_SelectGPS->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(GPSSelect::m_SelectGPSOnButtonClick), NULL, this);
}

void GPSSelect::m_GPSListUpdate()
{
	m_GPSList = COMPort::GetListPort();

	m_ListGPS->Clear();

	if (m_GPSList.size() == 0)
	{
		wxMessageBox("Адаптеры GPS не найдены.", this->GetTitle());
		return;
	}

	for (size_t i = 0; i < m_GPSList.size(); i++)
	{
		m_ListGPS->Append(m_GPSList[i].description);
	}
}

void GPSSelect::m_UpdateListOnButtonClick(wxCommandEvent& event)
{
	m_GPSListUpdate();
	m_GPSPort->Clear();
	m_baudrate->Clear();

	event.Skip();
}

void GPSSelect::m_ListGPSOnChoice(wxCommandEvent& event)
{
	int tempObject = m_ListGPS->GetSelection();

	if (tempObject == wxNOT_FOUND) return;

	m_GPSPort->SetLabel(m_GPSList[tempObject].port);
	m_baudrate->SetLabel("9600");

	event.Skip();
}

void GPSSelect::m_SelectGPSOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}