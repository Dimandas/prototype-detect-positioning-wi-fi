﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/choice.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class SelectWiFiAdapterDialog
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class SelectWiFiAdapterDialog : public wxDialog
	{
	private:

	protected:
		wxStaticText* m_staticText20;
		wxChoice* m_WiFiAdapterList;
		wxButton* m_updateAdapterList;
		wxButton* m_SelectAdapter;

		// Add user code
		void m_WiFiAdapterListUpdate();
		// End

		// Virtual event handlers, overide them in your derived class
		virtual void m_updateAdapterListOnButtonClick(wxCommandEvent& event);
		virtual void m_SelectAdapterOnButtonClick(wxCommandEvent& event);


	public:

		SelectWiFiAdapterDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Выбрать Wi-Fi адаптер"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(700, 253), long style = wxDEFAULT_DIALOG_STYLE);
		~SelectWiFiAdapterDialog();

		std::string GetSelectAdapter()
		{
			int temp = m_WiFiAdapterList->GetSelection();

			if (temp == wxNOT_FOUND) return "";

			return (std::string)(m_WiFiAdapterList->GetString(temp));
		}

	};
}