/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/choice.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class AverageData
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class AverageData : public wxDialog
	{
	private:

	protected:
		wxStaticText* m_staticText6;
		wxChoice* m_choiceGPS;
		wxStaticText* m_staticText7;
		wxChoice* m_choicePWR;
		wxStaticText* m_staticText8;
		wxChoice* m_choiceFindCoord;
		wxStaticText* m_staticText9;
		wxTextCtrl* m_textCtrlCountAverage;
		wxStaticText* m_staticText10;
		wxTextCtrl* m_textCtrlSizeBuff;
		wxButton* m_select;

		// Virtual event handlers, overide them in your derived class
		virtual void m_selectOnButtonClick(wxCommandEvent& event);


	public:

		AverageData(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("���������� ������"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(647, 430), long style = wxDEFAULT_DIALOG_STYLE);
		~AverageData();

		// 0 - �� ��������� ������
		// 1 - ������� ��������������
		// 2 - ������� �������������� � �������
		char GetAverageModeGPS()
		{
			int temp = m_choiceGPS->GetSelection();

			if (temp == wxNOT_FOUND) return 0;

			return temp;
		}

		// 0 - �� ��������� ������
		// 1 - �� ������������
		// 2 - ������� ��������������
		// 3 - ������� �������������� � �������
		char GetAverageModePWR()
		{
			int temp = m_choicePWR->GetSelection();

			if (temp == wxNOT_FOUND) return 0;

			return temp;
		}

		// 0 - �� ��������� ������
		// 1 - ������� ��������������
		// 2 - ������� �������������� � �������
		char GetAverageModeFindCoord()
		{
			int temp = m_choiceFindCoord->GetSelection();

			if (temp == wxNOT_FOUND) return 0;

			return temp;
		}

		size_t GetCountAverage()
		{
			wxString temp = m_textCtrlCountAverage->GetLineText(0);
			if (temp == "") return 0;

			int out = 0;
			try
			{
				out = std::stoi((std::string)temp);
			}
			catch (...)
			{
			}

			return out;
		}

		size_t GetSizeBuffAverage()
		{
			wxString temp = m_textCtrlSizeBuff->GetLineText(0);
			if (temp == "") return 0;

			int out = 0;
			try
			{
				out = std::stoi((std::string)temp);
			}
			catch (...)
			{
			}

			return out;
		}

	};
}