﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "inputcoord.h"

#include <wx/valtext.h>

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

InputCoordinatesDialog::InputCoordinatesDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	m_staticText1 = new wxStaticText(this, wxID_ANY, wxT("Текущий компьютер"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText1->Wrap(-1);
	bSizer2->Add(m_staticText1, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText2 = new wxStaticText(this, wxID_ANY, wxT("Широта: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText2->Wrap(-1);
	bSizer4->Add(m_staticText2, 0, wxALL, 5);

	// add user code
	wxTextValidator valid(wxFILTER_INCLUDE_CHAR_LIST, nullptr);
	valid.SetCharIncludes("0123456789.");
	// end

	m_inputLatitude0 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_inputLatitude0->HasFlag(wxTE_MULTILINE))
	{
		m_inputLatitude0->SetMaxLength(20);
	}
#else
	m_inputLatitude0->SetMaxLength(20);
#endif
	bSizer4->Add(m_inputLatitude0, 0, wxALL, 5);


	bSizer4->Add(0, 0, 1, wxEXPAND, 5);

	m_staticText3 = new wxStaticText(this, wxID_ANY, wxT("Долгота: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText3->Wrap(-1);
	bSizer4->Add(m_staticText3, 0, wxALL, 5);

	m_inputLongitude0 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_inputLongitude0->HasFlag(wxTE_MULTILINE))
	{
		m_inputLongitude0->SetMaxLength(20);
	}
#else
	m_inputLongitude0->SetMaxLength(20);
#endif
	bSizer4->Add(m_inputLongitude0, 0, wxALL, 5);


	bSizer2->Add(bSizer4, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer(wxHORIZONTAL);

	m_checkBoxDekart = new wxCheckBox(this, wxID_ANY, wxT("Декартовы координаты"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer41->Add(m_checkBoxDekart, 0, wxALIGN_BOTTOM | wxALL, 5);


	bSizer41->Add(0, 0, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer41, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer2, 1, wxEXPAND, 5);


	bSizer1->Add(0, 0, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);


	bSizer3->Add(0, 0, 1, wxEXPAND, 5);

	m_AddCoord = new wxButton(this, wxID_ANY, wxT("Добавить"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer3->Add(m_AddCoord, 0, wxALL | wxEXPAND, 5);


	bSizer1->Add(bSizer3, 1, wxEXPAND, 5);


	this->SetSizer(bSizer1);
	this->Layout();

	this->Centre(wxBOTH);

	// Connect Events
	m_checkBoxDekart->Connect(wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler(InputCoordinatesDialog::m_checkBoxDekartOnCheckBox), NULL, this);
	m_AddCoord->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(InputCoordinatesDialog::m_AddCoordOnButtonClick), NULL, this);
}

InputCoordinatesDialog::~InputCoordinatesDialog()
{
	// Disconnect Events
	m_checkBoxDekart->Disconnect(wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler(InputCoordinatesDialog::m_checkBoxDekartOnCheckBox), NULL, this);
	m_AddCoord->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(InputCoordinatesDialog::m_AddCoordOnButtonClick), NULL, this);
}

void InputCoordinatesDialog::m_AddCoordOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}

void InputCoordinatesDialog::m_checkBoxDekartOnCheckBox(wxCommandEvent& event)
{
	if (m_checkBoxDekart->IsChecked() == true)
	{
		m_staticText2->SetLabel("X: ");
		m_staticText3->SetLabel("Y: ");
	}
	else
	{
		m_staticText2->SetLabel("Широта: ");
		m_staticText3->SetLabel("Долгота: ");
	}

	event.Skip();
}