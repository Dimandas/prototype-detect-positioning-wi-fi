﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class AddComp
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class AddComp : public wxDialog
	{
	private:

	protected:
		wxStaticText* m_staticText1;
		wxTextCtrl* m_ipPortComp1;
		wxStaticText* m_staticText11;
		wxTextCtrl* m_ipPortComp2;
		wxStaticText* m_staticText12;
		wxTextCtrl* m_ipPortComp3;
		wxStaticText* m_staticText13;
		wxTextCtrl* m_ipPortComp4;
		wxStaticText* m_staticText14;
		wxTextCtrl* m_ipPortServer;
		wxButton* m_AddComp;

		// Virtual event handlers, overide them in your derived class
		virtual void m_AddCompOnButtonClick(wxCommandEvent& event);


	public:

		AddComp(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Добавить компьютер"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(498, 393), long style = wxDEFAULT_DIALOG_STYLE);
		~AddComp();

		inline std::string GetIpAndPortComp1()
		{
			return (std::string)m_ipPortComp1->GetLineText(0);
		}

		inline std::string GetIpAndPortComp2()
		{
			return (std::string)m_ipPortComp2->GetLineText(0);
		}

		inline std::string GetIpAndPortComp3()
		{
			return (std::string)m_ipPortComp3->GetLineText(0);
		}

		/*inline std::string GetIpAndPortComp4()
		{
			return (std::string)m_ipPortComp4->GetLineText(0);
		}*/

		inline std::string GetIpAndPortServer()
		{
			return (std::string)m_ipPortServer->GetLineText(0);
		}
	};
}
