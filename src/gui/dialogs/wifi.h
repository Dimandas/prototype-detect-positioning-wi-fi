﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/choice.h>
#include <wx/sizer.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/dialog.h>

#include "../../wifi/wlan.h"

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class WiFiSelect
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class WiFiSelect : public wxDialog
	{
	private:

	protected:
		wxStaticText* m_staticText5;
		wxChoice* m_WiFiList;
		wxButton* m_ScanWiFi;
		wxButton* m_SelectWiFi;

		// Add user code
		std::vector <Wlan::WiFiPoint> m_GetWiFiList;
		std::wstring m_SelectAdapter;
		bool m_lockScan = false;
		void m_UpdateWiFiList();
		// End

		// Virtual event handlers, overide them in your derived class
		virtual void m_ScanWiFiOnButtonClick(wxCommandEvent& event);
		virtual void m_SelectWiFiOnButtonClick(wxCommandEvent& event);


	public:

		WiFiSelect(std::wstring inputAdapter, wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Выбрать Wi-Fi точку"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(700, 335), long style = wxDEFAULT_DIALOG_STYLE);
		~WiFiSelect();

		std::string GetSSIDWiFi()
		{
			int temp = m_WiFiList->GetSelection();

			if (temp == wxNOT_FOUND) return "";

			return (std::string)(m_GetWiFiList[temp].SSID);
		}
		std::string GetMACWiFi()
		{
			int temp = m_WiFiList->GetSelection();

			if (temp == wxNOT_FOUND) return "";

			return (std::string)(m_GetWiFiList[temp].MAC);
		}
		std::string GetVisibleMACWiFi()
		{
			int temp = m_WiFiList->GetSelection();

			if (temp == wxNOT_FOUND) return "";

			return (std::string)(m_GetWiFiList[temp].visibleMAC);
		}
	};
}

