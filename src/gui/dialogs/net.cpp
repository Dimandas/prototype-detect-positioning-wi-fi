﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "net.h"

#include <wx/valtext.h>

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

AddComp::AddComp(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer(wxHORIZONTAL);

	// add user code
	wxTextValidator valid(wxFILTER_INCLUDE_CHAR_LIST, nullptr);
	valid.SetCharIncludes("0123456789.");
	// end

	m_staticText1 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 1 (IPv4):"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText1->Wrap(-1);
	bSizer4->Add(m_staticText1, 0, wxALL, 5);


	bSizer4->Add(0, 0, 1, wxEXPAND, 5);

	m_ipPortComp1 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_ipPortComp1->HasFlag(wxTE_MULTILINE))
	{
		m_ipPortComp1->SetMaxLength(15);
	}
#else
	m_ipPortComp1->SetMaxLength(15);
#endif
	bSizer4->Add(m_ipPortComp1, 0, wxALL, 5);


	bSizer2->Add(bSizer4, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText11 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 2 (IPv4):"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText11->Wrap(-1);
	bSizer41->Add(m_staticText11, 0, wxALL, 5);


	bSizer41->Add(0, 0, 1, wxEXPAND, 5);

	m_ipPortComp2 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_ipPortComp2->HasFlag(wxTE_MULTILINE))
	{
		m_ipPortComp2->SetMaxLength(15);
	}
#else
	m_ipPortComp2->SetMaxLength(15);
#endif
	bSizer41->Add(m_ipPortComp2, 0, wxALL, 5);


	bSizer2->Add(bSizer41, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText12 = new wxStaticText(this, wxID_ANY, wxT("Компьютер 3 (IPv4):"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText12->Wrap(-1);
	bSizer42->Add(m_staticText12, 0, wxALL, 5);


	bSizer42->Add(0, 0, 1, wxEXPAND, 5);

	m_ipPortComp3 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_ipPortComp3->HasFlag(wxTE_MULTILINE))
	{
		m_ipPortComp3->SetMaxLength(15);
	}
#else
	m_ipPortComp3->SetMaxLength(15);
#endif
	bSizer42->Add(m_ipPortComp3, 0, wxALL, 5);


	bSizer2->Add(bSizer42, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer43;
	bSizer43 = new wxBoxSizer(wxHORIZONTAL);

	/*
	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Компьютер 4 (IPv4):"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap(-1);
	bSizer43->Add(m_staticText13, 0, wxALL, 5); */


	bSizer43->Add(0, 0, 1, wxEXPAND, 5);

	//m_ipPortComp4 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0);
	//bSizer43->Add(m_ipPortComp4, 0, wxALL, 5);


	bSizer2->Add(bSizer43, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer44;
	bSizer44 = new wxBoxSizer(wxHORIZONTAL);


	bSizer44->Add(0, 0, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer44, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer45;
	bSizer45 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText14 = new wxStaticText(this, wxID_ANY, wxT("Сервер (необязательно):"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText14->Wrap(-1);
	bSizer45->Add(m_staticText14, 0, wxALL, 5);


	bSizer45->Add(0, 0, 1, wxEXPAND, 5);

	m_ipPortServer = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(180, -1), 0, valid);
#ifdef __WXGTK__
	if (!m_ipPortServer->HasFlag(wxTE_MULTILINE))
	{
		m_ipPortServer->SetMaxLength(15);
	}
#else
	m_ipPortServer->SetMaxLength(15);
#endif
	bSizer45->Add(m_ipPortServer, 0, wxALL, 5);


	bSizer2->Add(bSizer45, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer2, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);


	bSizer3->Add(0, 0, 1, wxEXPAND, 5);

	m_AddComp = new wxButton(this, wxID_ANY, wxT("Добавить"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer3->Add(m_AddComp, 0, wxALL | wxEXPAND, 5);


	bSizer1->Add(bSizer3, 1, wxEXPAND, 5);


	this->SetSizer(bSizer1);
	this->Layout();

	this->Centre(wxBOTH);

	// Connect Events
	m_AddComp->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AddComp::m_AddCompOnButtonClick), NULL, this);
}

AddComp::~AddComp()
{
	// Disconnect Events
	m_AddComp->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AddComp::m_AddCompOnButtonClick), NULL, this);
}

void AddComp::m_AddCompOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}