﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wifi.h"

#include <wx/msgdlg.h>

using namespace DetectPositioningWiFi;

///////////////////////////////////////////////////////////////////////////

WiFiSelect::WiFiSelect(std::wstring inputAdapter, wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText5 = new wxStaticText(this, wxID_ANY, wxT("Выбрать из списка:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	bSizer10->Add(m_staticText5, 0, wxALL, 5);


	bSizer10->Add(0, 0, 1, wxEXPAND, 5);

	wxArrayString m_WiFiListChoices;
	m_WiFiList = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(500, -1), m_WiFiListChoices, 0);
	m_WiFiList->SetSelection(0);
	bSizer10->Add(m_WiFiList, 0, wxALL, 5);


	bSizer8->Add(bSizer10, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer(wxVERTICAL);

	m_ScanWiFi = new wxButton(this, wxID_ANY, wxT("Сканировать"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer13->Add(m_ScanWiFi, 0, wxALIGN_CENTER | wxALL, 5);


	bSizer12->Add(bSizer13, 1, wxEXPAND, 5);


	bSizer8->Add(bSizer12, 1, wxEXPAND, 5);


	bSizer7->Add(bSizer8, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer(wxVERTICAL);


	bSizer9->Add(0, 0, 1, wxEXPAND, 5);

	m_SelectWiFi = new wxButton(this, wxID_ANY, wxT("Выбрать"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer9->Add(m_SelectWiFi, 0, wxALL | wxEXPAND, 5);


	bSizer7->Add(bSizer9, 1, wxEXPAND, 5);


	this->SetSizer(bSizer7);
	this->Layout();

	this->Centre(wxBOTH);

	// Add user code
	m_SelectAdapter = inputAdapter;
	m_UpdateWiFiList();
	// End

	// Connect Events
	m_ScanWiFi->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(WiFiSelect::m_ScanWiFiOnButtonClick), NULL, this);
	m_SelectWiFi->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(WiFiSelect::m_SelectWiFiOnButtonClick), NULL, this);
}

WiFiSelect::~WiFiSelect()
{
	// Disconnect Events
	m_ScanWiFi->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(WiFiSelect::m_ScanWiFiOnButtonClick), NULL, this);
	m_SelectWiFi->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(WiFiSelect::m_SelectWiFiOnButtonClick), NULL, this);
}

void WiFiSelect::m_UpdateWiFiList()
{
	if (m_lockScan == true)
		return;

	m_lockScan = true;

	if (m_SelectAdapter == "")
	{
		wxMessageBox(wxT("Не выбран Wi-Fi адаптер."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);
		m_lockScan = false;
		return;
	}

	m_GetWiFiList.clear();
	m_WiFiList->Clear();

	try
	{
		m_GetWiFiList = Wlan::GetListWiFi(m_SelectAdapter);
	}
	catch (...)
	{
	}

	for (size_t i = 0; i < m_GetWiFiList.size(); i++)
	{
		m_WiFiList->Append(m_GetWiFiList[i].SSID + "    (PWR: " + std::to_string(m_GetWiFiList[i].PWR) + ")" + "    (MAC: " + m_GetWiFiList[i].visibleMAC + ")");
	}

	m_lockScan = false;
}

void WiFiSelect::m_ScanWiFiOnButtonClick(wxCommandEvent& event)
{
	m_UpdateWiFiList();
	event.Skip();
}

void WiFiSelect::m_SelectWiFiOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}