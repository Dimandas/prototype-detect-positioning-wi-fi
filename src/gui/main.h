﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/hyperlink.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/menu.h>
#include <wx/frame.h>

#include <wx/socket.h>

#include <list>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class m_mainMenu
///////////////////////////////////////////////////////////////////////////////
namespace DetectPositioningWiFi
{
	class MainFrame : public wxFrame
	{
	private:
		std::string m_version = "1.2.0.0";

	protected:
		wxStaticText* m_staticText1;
		wxStaticText* m_staticTextCOMPort;
		wxStaticText* m_staticTextBaudrate;
		wxStaticText* m_staticTextLatitude;
		wxStaticText* m_staticTextLongitude;
		wxStaticText* m_staticTextAltitude;
		wxStaticText* m_staticText11;
		wxStaticText* m_staticTextWiFiAdapter;
		wxStaticText* m_staticTextSSID;
		wxStaticText* m_staticTextMAC;
		wxStaticText* m_staticTextPWR;
		wxStaticText* m_staticTextDistance;
		wxStaticText* m_staticText5;
		wxStaticText* m_staticTextComp1;
		wxStaticText* m_staticTextComp2;
		wxStaticText* m_staticTextComp3;
		wxStaticText* m_staticTextComp4;
		wxStaticText* m_staticTextServer;
		wxStaticText* m_staticText16;
		wxStaticText* m_staticTextLatitudeOut;
		wxStaticText* m_staticTextLongitudeOut;
		wxStaticText* m_staticTextAltitudeOut;
		wxStaticText* m_staticTextAzimuth;
		wxStaticText* m_staticTextCoordDistance;
		wxStaticText* m_staticTextAllStatus;
		wxButton* m_copy;
		wxHyperlinkCtrl* m_hyperlinkShowMap;

		wxStaticText* m_staticText20;
		wxStaticText* m_staticTextLatitudeCOMP1;
		wxStaticText* m_staticTextLongitudeCOMP1;
		wxStaticText* m_staticTextAltitudeCOMP1;
		wxStaticText* m_staticTextSSIDCOMP1;
		wxStaticText* m_staticTextMAC1;
		wxStaticText* m_staticTextPWRCOMP1;
		wxStaticText* m_staticTextDistance1;
		wxStaticText* m_staticText201;
		wxStaticText* m_staticTextLatitudeCOMP11;
		wxStaticText* m_staticTextLongitudeCOMP11;
		wxStaticText* m_staticTextAltitudeCOMP11;
		wxStaticText* m_staticTextSSIDCOMP11;
		wxStaticText* m_staticTextMAC2;
		wxStaticText* m_staticTextPWRCOMP11;
		wxStaticText* m_staticTextDistance2;
		wxStaticText* m_staticText202;
		wxStaticText* m_staticTextLatitudeCOMP12;
		wxStaticText* m_staticTextLongitudeCOMP12;
		wxStaticText* m_staticTextAltitudeCOMP12;
		wxStaticText* m_staticTextSSIDCOMP12;
		wxStaticText* m_staticTextMAC3;
		wxStaticText* m_staticTextPWRCOMP12;
		wxStaticText* m_staticTextDistance3;
		wxStaticText* m_staticText203;
		wxStaticText* m_staticTextLatitudeCOMP13;
		wxStaticText* m_staticTextLongitudeCOMP13;
		wxStaticText* m_staticTextAltitudeCOMP13;
		wxStaticText* m_staticTextSSIDCOMP13;
		wxStaticText* m_staticTextMAC4;
		wxStaticText* m_staticTextPWRCOMP13;
		wxStaticText* m_staticTextDistance4;

		wxButton* m_buttonStop;
		wxButton* m_buttonStart;
		wxMenuBar* m_menubar1;
		wxMenu* m_menuGPS;
		wxMenu* m_menuWiFi;
		wxMenu* m_menuNet;
		wxMenu* m_menuSettings;
		wxMenu* m_help;

		// Add user code
		wxBoxSizer* bSizer11;
		wxMenuItem* m_menuItemSelectGPS;
		wxMenuItem* m_menuItemSelectWiFiAdapter;
		wxMenuItem* m_menuItemSelectWiFi;

		std::string m_TextCOMPort;
		std::string m_TextBaudrate;
		std::string m_TextLatitude;
		std::string m_TextLongitude;
		std::string m_TextAltitude;
		std::string m_TextAdapter;
		std::string m_TextSSID;
		std::string m_TextMAC;
		std::string m_TextPWR;
		std::string m_TextDistance;
		std::string m_TextComp1;
		std::string m_TextComp2;
		std::string m_TextComp3;
		std::string m_TextComp4;
		std::string m_TextServer;
		std::string m_TextLatitudeOut;
		std::string m_TextLongitudeOut;
		std::string m_TextAltitudeOut;
		std::string m_TextAzimuth;
		std::string m_TextAllStatus;

		std::string m_Textlatitude1;
		std::string m_Textlongitude1;
		std::string m_Textaltitude1;
		std::string m_TextSSID1;
		std::string m_TextMAC1;
		std::string m_TextwifiPWR1;

		std::string m_Textlatitude2;
		std::string m_Textlongitude2;
		std::string m_Textaltitude2;
		std::string m_TextSSID2;
		std::string m_TextMAC2;
		std::string m_TextwifiPWR2;

		std::string m_Textlatitude3;
		std::string m_Textlongitude3;
		std::string m_Textaltitude3;
		std::string m_TextSSID3;
		std::string m_TextMAC3;
		std::string m_TextwifiPWR3;

		std::string m_Textlatitude4;
		std::string m_Textlongitude4;
		std::string m_Textaltitude4;
		std::string m_TextSSID4;
		std::string m_TextMAC4;
		std::string m_TextwifiPWR4;

		std::string m_TextX = "X: ";
		std::string m_TextY = "Y: ";

		void m_updateLabel();

		//
		bool m_coordEntered = false;
		bool m_useDekart = false;

		char AverageModeGPS = 0;
		char AverageModePWR = 0;
		char AverageModeFindCoord = 0;
		size_t AverageCount = 0;
		size_t AverageSizeBuff = 0;

		std::string m_allStatus = "";
		//
		size_t countGPSCoord = 0;
		double FindAverageGPSCoord1(double newNumber, size_t count);
		std::list<double> averageGPSCoord1;
		double FindAverageGPSCoord2(double newNumber, size_t count);
		std::list<double> averageGPSCoord2;
		void ClearAverageGPSCoord();

		double FindAverageBuffGPSCoord1(double newNumber);
		std::list<double> averageBuffGPSCoord1;
		double FindAverageBuffGPSCoord2(double newNumber);
		std::list<double> averageBuffGPSCoord2;
		void ClearAverageBuffGPSCoord();
		//

		size_t countPWR = 0;
		double FindMinimumPWR(double newNumber, double previousNumber);
		double minimumPWR = std::numeric_limits<double>::max();
		void ClearMinimumPWR();

		double FindAveragePWR(double newNumber, size_t count);
		std::list<double> averagePWR;
		void ClearAveragePWR();

		double FindAverageBuffPWR(double newNumber);
		std::list<double> averageBuffPWR;
		void ClearAverageBuffPWR();
		//

		size_t countFindCoord = 0;
		double FindAverageFindCoord1(double newNumber, size_t count);
		std::list<double> averageFindCoord1;
		double FindAverageFindCoord2(double newNumber, size_t count);
		std::list<double> averageFindCoord2;
		void ClearAverageFindCoord();

		double FindAverageBuffFindCoord1(double newNumber);
		std::list<double> averageBuffFindCoord1;
		double FindAverageBuffFindCoord2(double newNumber);
		std::list<double> averageBuffFindCoord2;
		void ClearAverageBuffFindCoord();
		//

		//

		std::string m_comPort;
		int m_comBaudrate = 0;
		double m_latitude = 0;
		double m_longitude = 0;
		double m_altitude = 0;
		bool m_coordUsed = false;

		std::string m_wifiAdapter;
		std::string m_wifiSSID;
		std::string m_wifiMAC;
		std::string m_wifiVisibleMAC = "";
		double m_wifiPWR = 0;
		double m_wifiDistance = 0;
		//

		// Add network code
		wxDatagramSocket* sock1;
		wxDatagramSocket* sock2;
		wxDatagramSocket* sock3;
		wxDatagramSocket* sock4;

		std::string m_ipPortComp1;
		std::string m_ipStatus1 = "";
		std::string m_ipPortComp2;
		std::string m_ipStatus2 = "";
		std::string m_ipPortComp3;
		std::string m_ipStatus3 = "";
		std::string m_ipPortComp4;
		std::string m_ipStatus4 = "";
		std::string m_ipPortServer;

		double m_latitude1 = 0;
		double m_longitude1 = 0;
		double m_altitude1 = 0;
		bool m_coordUsed1 = false;
		std::string m_SSID1 = "";
		std::string m_wifiVisibleMAC1 = "";
		double m_wifiPWR1 = 0;
		double m_wifiDistance1 = 0;

		double m_latitude2 = 0;
		double m_longitude2 = 0;
		double m_altitude2 = 0;
		bool m_coordUsed2 = false;
		std::string m_SSID2 = "";
		std::string m_wifiVisibleMAC2 = "";
		double m_wifiPWR2 = 0;
		double m_wifiDistance2 = 0;

		double m_latitude3 = 0;
		double m_longitude3 = 0;
		double m_altitude3 = 0;
		bool m_coordUsed3 = false;
		std::string m_SSID3 = "";
		std::string m_wifiVisibleMAC3 = "";
		double m_wifiPWR3 = 0;
		double m_wifiDistance3 = 0;

		double m_latitude4 = 0;
		double m_longitude4 = 0;
		double m_altitude4 = 0;
		bool m_coordUsed4 = false;
		std::string m_SSID4 = "";
		std::string m_wifiVisibleMAC4 = "";
		double m_wifiPWR4 = 0;
		double m_wifiDistance4 = 0;

		void SendData(std::string ip, std::string port, double latitude, double longitude, double altitude, double wifiPWR, double wifiFreq, std::string wifiVisibleMAC, std::string wifiSSID);
		void NetworkClearAllData();
		//

		double m_outLatitude = 0;
		double m_outLongitude = 0;
		double m_outAltitude = 0;
		double m_outAzimuth = 0;
		double m_outCoordDistance = 0;
		//

		bool m_runThread = false;
		bool m_stopThread = false;
		bool m_exit = false;
		bool m_lockSelectWiFiDialog = false;
		// End

		// Virtual event handlers, overide them in your derived class
		virtual void MainFrameOnClose(wxCloseEvent& event);
		// TODO
		virtual void MainFrameOnHibernate(wxActivateEvent& event) { event.Skip(); }
		virtual void m_copyOnButtonClick(wxCommandEvent& event);
		virtual void m_buttonStopOnButtonClick(wxCommandEvent& event);
		virtual void m_buttonStartOnButtonClick(wxCommandEvent& event);
		virtual void m_menuItemSelectGPSOnMenuSelection(wxCommandEvent& event);
		virtual void m_menuItemInputCoordOnMenuSelection(wxCommandEvent& event);
		virtual void m_menuItemSelectWiFiAdapterOnMenuSelection(wxCommandEvent& event);
		virtual void m_menuItemSelectWiFiOnMenuSelection(wxCommandEvent& event);
		virtual void m_menuItemAddCompOnMenuSelection(wxCommandEvent& event);
		virtual void m_menuItemAverDataOnMenuSelection(wxCommandEvent& event);
		virtual void m_aboutOnMenuSelection(wxCommandEvent& event);

		void OnSocketEvent1(wxSocketEvent& event);
		void OnSocketEvent2(wxSocketEvent& event);
		void OnSocketEvent3(wxSocketEvent& event);
		void OnSocketEvent4(wxSocketEvent& event);

	public:

		MainFrame(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Prototype Detect Positioning Wi-Fi"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(1147, 312), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

		~MainFrame();

		wxDECLARE_EVENT_TABLE();
	};
}