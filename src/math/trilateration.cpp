﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "trilateration.h"

#include <cmath>

#include <GeographicLib/LambertConformalConic.hpp>
#include <GeographicLib/Geodesic.hpp>

using namespace DetectPositioningWiFi;

double Trilateration::CalculateDistance(double ipwr, int freq)
{
	double exp1 = (double)((27.55 - (20 * std::log10(freq / 1000)) + std::abs(ipwr)) / 20.0);

	return (double)std::pow(10.0, exp1);
}

double Trilateration::m_centralMeridian; // костыль

Trilateration::Position Trilateration::FindPosition(bool coordUse1, double latitude1, double longitude1, double pwr1, bool coordUse2, double latitude2, double longitude2, double pwr2, bool coordUse3, double latitude3, double longitude3, double pwr3, bool coordUse4, double latitude4, double longitude4, double pwr4, bool coordUse5, double latitude5, double longitude5, double pwr5, int freq)
{
	double radius1 = CalculateDistance(pwr1, freq);
	double radius2 = CalculateDistance(pwr2, freq);
	double radius3 = CalculateDistance(pwr3, freq);
	double radius4 = CalculateDistance(pwr4, freq);
	//double radius5 = CalculateDistance(pwr5, freq); не используется

	//
	m_centralMeridian = longitude1;
	//
	double x1 = 0;
	double y1 = 0;
	GeographicLib::LambertConformalConic::Mercator().Forward(m_centralMeridian, latitude1, longitude1, x1, y1);
	//
	double x2 = 0;
	double y2 = 0;
	if (coordUse2 == true)
	{
		GeographicLib::LambertConformalConic::Mercator().Forward(m_centralMeridian, latitude2, longitude2, x2, y2);
	}
	//
	double x3 = 0;
	double y3 = 0;
	if (coordUse3 == true)
	{
		GeographicLib::LambertConformalConic::Mercator().Forward(m_centralMeridian, latitude3, longitude3, x3, y3);
	}
	//
	double x4 = 0;
	double y4 = 0;
	if (coordUse4 == true)
	{
		GeographicLib::LambertConformalConic::Mercator().Forward(m_centralMeridian, latitude4, longitude4, x4, y4);
	}
	//

	double tempDistance = 0;
	GeographicLib::Geodesic calc(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
	std::vector <Point> findPoints;
	findPoints.reserve(10);

	if (coordUse1 == true)
	{
		if (coordUse2 == true)
		{
			calc.Inverse(latitude1, longitude1, latitude2, longitude2, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x1, y1, radius1, x2, y2, radius2, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
		if (coordUse3 == true)
		{
			calc.Inverse(latitude1, longitude1, latitude3, longitude3, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x1, y1, radius1, x3, y3, radius3, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
		if (coordUse4 == true)
		{
			calc.Inverse(latitude1, longitude1, latitude4, longitude4, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x1, y1, radius1, x4, y4, radius4, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
	}
	//
	if (coordUse2 == true)
	{
		if (coordUse3 == true)
		{
			calc.Inverse(latitude2, longitude2, latitude3, longitude3, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x2, y2, radius2, x3, y3, radius3, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
		if (coordUse4 == true)
		{
			calc.Inverse(latitude2, longitude2, latitude4, longitude4, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x2, y2, radius2, x4, y4, radius4, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
	}
	//
	if (coordUse3 == true)
	{
		if (coordUse4 == true)
		{
			calc.Inverse(latitude3, longitude3, latitude4, longitude4, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x3, y3, radius3, x4, y4, radius4, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
	}
	//
	if (findPoints.size() == 0)
	{
		throw std::runtime_error("Not found the point of intersection");
	}

	tempDistance = 0;
	double tempLatitude = 0;
	double tempLongitude = 0;

	for (size_t i = 0; i < findPoints.size(); i++)
	{
		GeographicLib::LambertConformalConic::Mercator().Reverse(m_centralMeridian, findPoints[i].x, findPoints[i].y, tempLatitude, tempLongitude);

		if (coordUse1 == true)
		{
			calc.Inverse(latitude1, longitude1, tempLatitude, tempLongitude, tempDistance);
			if (tempDistance > radius1)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
		if (coordUse2 == true)
		{
			calc.Inverse(latitude2, longitude2, tempLatitude, tempLongitude, tempDistance);
			if (tempDistance > radius2)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
		if (coordUse3 == true)
		{
			calc.Inverse(latitude3, longitude3, tempLatitude, tempLongitude, tempDistance);
			if (tempDistance > radius3)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
		if (coordUse4 == true)
		{
			calc.Inverse(latitude4, longitude4, tempLatitude, tempLongitude, tempDistance);
			if (tempDistance > radius4)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
	}
	//
	if (findPoints.size() == 0)
	{
		throw std::runtime_error("No common points found");
	}

	double outLatitude = 0;
	double outLongitude = 0;

	Point tempOut = FindCenterPolygon(findPoints);
	GeographicLib::LambertConformalConic::Mercator().Reverse(m_centralMeridian, tempOut.x, tempOut.y, outLatitude, outLongitude);

	return Position(outLatitude, outLongitude);
}

std::vector<Trilateration::Point> Trilateration::FindIntersectionCircles(double x1, double y1, double r1, double x2, double y2, double r2, double distance)
{
	std::vector<Point> out;

	if (distance == 0 && r1 == r2)
	{
		//throw std::runtime_error("there are no solutions: circles are identical (are one and the same circumference)");
		return out;
	}

	if (distance > r1 + r2)
	{
		//throw std::runtime_error("there are no solutions: the circles are separate");
		return out;
	}

	if (distance < std::abs(r1 - r2))
	{
		//throw std::runtime_error("there are no solutions: because one circle is inside the other");
		return out;
	}

	double a = (std::pow(r1, 2) - std::pow(r2, 2) + std::pow(distance, 2)) / (2 * distance);
	//
	double h = std::sqrt(std::pow(r1, 2) - std::pow(a, 2));

	//
	double x3 = x1 + a * (x2 - x1) / distance;
	double y3 = y1 + a * (y2 - y1) / distance;

	double x4 = x3 + h * (y2 - y1) / distance;
	double y4 = y3 - h * (x2 - x1) / distance;
	//
	double x5 = x3 - h * (y2 - y1) / distance;
	double y5 = y3 + h * (x2 - x1) / distance;
	//

	out.push_back(Point(x4, y4));
	out.push_back(Point(x5, y5));

	return out;
}

Trilateration::Point Trilateration::FindCenterPolygon(std::vector<Trilateration::Point> inputPoints)
{
	double sumX = 0;
	double sumY = 0;

	for (size_t i = 0; i < inputPoints.size(); i++)
	{
		sumX = sumX + inputPoints[i].x;
		sumY = sumY + inputPoints[i].y;
	}
	sumX = sumX / inputPoints.size();
	sumY = sumY / inputPoints.size();

	return Point(sumX, sumY);
}

void Trilateration::FindDekartDistance(double x1, double y1, double x2, double y2, double& distance)
{
	distance = (double)std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2));
}

Trilateration::Position Trilateration::FindDekartPosition(bool coordUse1, double x1, double y1, double pwr1, bool coordUse2, double x2, double y2, double pwr2, bool coordUse3, double x3, double y3, double pwr3, bool coordUse4, double x4, double y4, double pwr4, bool coordUse5, double x5, double y5, double pwr5, int freq)
{
	double radius1 = CalculateDistance(pwr1, freq);
	double radius2 = CalculateDistance(pwr2, freq);
	double radius3 = CalculateDistance(pwr3, freq);
	double radius4 = CalculateDistance(pwr4, freq);
	//double radius5 = CalculateDistance(pwr5, freq); не используется

	double tempDistance = 0;
	std::vector <Point> findPoints;
	findPoints.reserve(10);

	if (coordUse1 == true)
	{
		if (coordUse2 == true)
		{
			FindDekartDistance(x1, y1, x2, y2, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x1, y1, radius1, x2, y2, radius2, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
		if (coordUse3 == true)
		{
			FindDekartDistance(x1, y1, x3, y3, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x1, y1, radius1, x3, y3, radius3, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
		if (coordUse4 == true)
		{
			FindDekartDistance(x1, y1, x4, y4, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x1, y1, radius1, x4, y4, radius4, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
	}
	//
	if (coordUse2 == true)
	{
		if (coordUse3 == true)
		{
			FindDekartDistance(x2, y2, x3, y3, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x2, y2, radius2, x3, y3, radius3, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
		if (coordUse4 == true)
		{
			FindDekartDistance(x2, y2, x4, y4, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x2, y2, radius2, x4, y4, radius4, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
	}
	//
	if (coordUse3 == true)
	{
		if (coordUse4 == true)
		{
			FindDekartDistance(x3, y3, x4, y4, tempDistance);
			std::vector <Point> temp = FindIntersectionCircles(x3, y3, radius3, x4, y4, radius4, tempDistance);
			for (size_t i = 0; i < temp.size(); i++)
			{
				findPoints.push_back(temp[i]);
			}
		}
		//
	}
	//
	if (findPoints.size() == 0)
	{
		throw std::runtime_error("Not found the point of intersection");
	}

	tempDistance = 0;

	for (size_t i = 0; i < findPoints.size(); i++)
	{
		if (coordUse1 == true)
		{
			FindDekartDistance(x1, y1, findPoints[i].x, findPoints[i].y, tempDistance);
			if (tempDistance > radius1)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
		if (coordUse2 == true)
		{
			FindDekartDistance(x2, y2, findPoints[i].x, findPoints[i].y, tempDistance);
			if (tempDistance > radius2)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
		if (coordUse3 == true)
		{
			FindDekartDistance(x3, y3, findPoints[i].x, findPoints[i].y, tempDistance);
			if (tempDistance > radius3)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
		if (coordUse4 == true)
		{
			FindDekartDistance(x4, y4, findPoints[i].x, findPoints[i].y, tempDistance);
			if (tempDistance > radius4)
			{
				findPoints.erase(findPoints.cbegin() + i);
				i = i - 1;
				continue;
			}
		}
		//
	}
	//
	if (findPoints.size() == 0)
	{
		throw std::runtime_error("No common points found");
	}

	Point tempOut = FindCenterPolygon(findPoints);

	return Position(tempOut.x, tempOut.y);
}

void Trilateration::FindCoordDistance(double latitude1, double longitude1, double latitude2, double longitude2, double& distance)
{
	GeographicLib::Geodesic calc(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());

	calc.Inverse(latitude1, longitude1, latitude2, longitude2, distance);
}

void Trilateration::FindCoordAzimuth(double latitude1, double longitude1, double latitude2, double longitude2, double& azimuth)
{
	GeographicLib::Geodesic calc(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());

	double temp = 0;
	calc.Inverse(latitude1, longitude1, latitude2, longitude2, azimuth, temp);
}