﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#pragma once

#include <vector>

namespace DetectPositioningWiFi
{
	class Trilateration
	{
	public:
		struct Position
		{
			Position(double _latitude, double _longitude)
			{
				latitude = _latitude;
				longitude = _longitude;
			}

			double latitude;
			double longitude;
		};

		static void FindCoordDistance(double latitude1, double longitude1, double latitude2, double longitude2, double& distance);
		static void FindCoordAzimuth(double latitude1, double longitude1, double latitude2, double longitude2, double& azimuth);
		static void FindDekartDistance(double x1, double y1, double x2, double y2, double& distance);
		static double CalculateDistance(double ipwr, int freq);
		static Position FindPosition(bool coordUse1, double latitude1, double longitude1, double pwr1, bool coordUse2, double latitude2, double longitude2, double pwr2, bool coordUse3, double latitude3, double longitude3, double pwr3, bool coordUse4, double latitude4, double longitude4, double pwr4, bool coordUse5, double latitude5, double longitude5, double pwr5, int freq);
		static Position FindDekartPosition(bool coordUse1, double x1, double y1, double pwr1, bool coordUse2, double x2, double y2, double pwr2, bool coordUse3, double x3, double y3, double pwr3, bool coordUse4, double x4, double y4, double pwr4, bool coordUse5, double x5, double y5, double pwr5, int freq);

	protected:
		struct Point
		{
			Point(double _x, double _y)
			{
				x = _x;
				y = _y;
			}

			double x;
			double y;
		};

		static std::vector<Point> FindIntersectionCircles(double x1, double y1, double r1, double x2, double y2, double r2, double distance);
		static Point FindCenterPolygon(std::vector<Point> inputPoints);

		static double m_centralMeridian;
	};
}