﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "wlan.h"

using namespace DetectPositioningWiFi;

bool Wlan::LockThread = true;

std::vector <std::wstring> Wlan::GetListAdapters()
{
	HANDLE handle = NULL;
	DWORD dword = NULL;

	if (WlanOpenHandle(2, NULL, &dword, &handle) != 0)
	{
		throw std::runtime_error("WlanOpenHandleError");
	}

	PWLAN_INTERFACE_INFO_LIST interfaceList = NULL;
	if (WlanEnumInterfaces(handle, NULL, &interfaceList) != 0)
	{
		WlanCloseHandle(handle, NULL);
		throw std::runtime_error("WlanEnumInterfacesError");
	}

	std::vector <std::wstring> out;

	for (size_t i = 0; i < interfaceList->dwNumberOfItems; i++)
	{
		out.push_back(interfaceList->InterfaceInfo[i].strInterfaceDescription);
	}

	WlanCloseHandle(handle, NULL);
	WlanFreeMemory(interfaceList);

	return out;
}

std::vector <Wlan::WiFiPoint> Wlan::GetListWiFi(std::wstring inputAdapter)
{
	std::vector <Wlan::WiFiPoint> out;

	HANDLE handle = NULL;
	DWORD dword = NULL;

	if (WlanOpenHandle(2, NULL, &dword, &handle) != 0)
	{
		throw std::runtime_error("WlanOpenHandleError");
	}

	PWLAN_INTERFACE_INFO_LIST interfaceList = NULL;
	if (WlanEnumInterfaces(handle, NULL, &interfaceList) != 0)
	{
		WlanCloseHandle(handle, NULL);
		throw std::runtime_error("WlanEnumInterfacesError");
	}

	// костыль
	bool found = false;
	size_t selectAdapter;

	for (size_t i = 0; i < interfaceList->dwNumberOfItems; i++)
	{
		if (interfaceList->InterfaceInfo[i].strInterfaceDescription == inputAdapter)
		{
			found = true;
			selectAdapter = i;
		}
	}

	if (found != true)
	{
		throw std::runtime_error("FindError");
	}

	DWORD PrevNot = 0;
	WlanRegisterNotification(
		handle,
		WLAN_NOTIFICATION_SOURCE_ACM,
		false,
		(WLAN_NOTIFICATION_CALLBACK)WiFiNotify,
		NULL,
		NULL,
		&PrevNot
	);

	if (WlanScan(handle, &interfaceList->InterfaceInfo[selectAdapter].InterfaceGuid, NULL, NULL, NULL) != 0)
	{
		throw std::runtime_error("WlanScanError");
	}

	while (LockThread == true)
	{
		SmartSleep(5, 5);
	}

	LockThread = true;

	WlanRegisterNotification(
		handle,
		WLAN_NOTIFICATION_SOURCE_NONE,
		false,
		(WLAN_NOTIFICATION_CALLBACK)WiFiNotify,
		NULL,
		NULL,
		&PrevNot
	);

	PWLAN_BSS_LIST wlanList;
	if (WlanGetNetworkBssList(handle, &interfaceList->InterfaceInfo[selectAdapter].InterfaceGuid, NULL, dot11_BSS_type_any, 0, NULL, &wlanList) != 0)
	{
		throw std::runtime_error("WlanGetNetworkBssListError");
	}

	for (size_t i = 0; i < wlanList->dwNumberOfItems; i++)
	{
		std::string readSSID = "";
		for (size_t sizeSSID = 0; sizeSSID < wlanList->wlanBssEntries[i].dot11Ssid.uSSIDLength; sizeSSID++)
		{
			readSSID = readSSID + (char)wlanList->wlanBssEntries[i].dot11Ssid.ucSSID[sizeSSID];
		}

		std::string readBSSID = "";
		for (size_t sizeBSSID = 0; sizeBSSID < 6; sizeBSSID++)
		{
			readBSSID = readBSSID + (char)wlanList->wlanBssEntries[i].dot11Bssid[sizeBSSID];
		}

		out.push_back(WiFiPoint(readSSID, readBSSID, ConvertDECToHEX(readBSSID), wlanList->wlanBssEntries[i].ulChCenterFrequency, wlanList->wlanBssEntries[i].lRssi));
	}

	WlanCloseHandle(handle, NULL);
	WlanFreeMemory(interfaceList);
	WlanFreeMemory(wlanList);

	return out;
}

Wlan::Wlan(std::wstring inputAdapter)
{
	Create(inputAdapter);
}

void Wlan::Create(std::wstring inputAdapter)
{
	HANDLE handle = NULL;
	DWORD dword = NULL;

	if (WlanOpenHandle(2, NULL, &dword, &handle) != 0)
	{
		throw std::runtime_error("WlanOpenHandleError");
	}

	PWLAN_INTERFACE_INFO_LIST interfaceList = NULL;
	if (WlanEnumInterfaces(handle, NULL, &interfaceList) != 0)
	{
		WlanCloseHandle(handle, NULL);
		throw std::runtime_error("WlanEnumInterfacesError");
	}

	// костыль
	bool found = false;
	size_t selectAdapter;

	for (size_t i = 0; i < interfaceList->dwNumberOfItems; i++)
	{
		if (interfaceList->InterfaceInfo[i].strInterfaceDescription == inputAdapter)
		{
			found = true;
			selectAdapter = i;
		}
	}
	if (found != true)
	{
		throw std::runtime_error("FindError");
	}

	m_wlanHandle = handle;
	m_wlanGUID = interfaceList->InterfaceInfo[selectAdapter];

	WlanFreeMemory(interfaceList);
}

Wlan::~Wlan()
{
	WlanCloseHandle(m_wlanHandle, NULL);
}

Wlan::WiFiPoint Wlan::GetInfoWiFi(std::string MAC)
{
	DWORD PrevNot = 0;
	WlanRegisterNotification(
		m_wlanHandle,
		WLAN_NOTIFICATION_SOURCE_ACM,
		false,
		(WLAN_NOTIFICATION_CALLBACK)WiFiNotify,
		NULL,
		NULL,
		&PrevNot
	);

	if (WlanScan(m_wlanHandle, &m_wlanGUID.InterfaceGuid, NULL, NULL, NULL) != 0)
	{
		throw std::runtime_error("WlanScanError");
	}

	while (LockThread == true)
	{
		SmartSleep(5, 5);
	}

	LockThread = true;

	WlanRegisterNotification(
		m_wlanHandle,
		WLAN_NOTIFICATION_SOURCE_NONE,
		false,
		(WLAN_NOTIFICATION_CALLBACK)WiFiNotify,
		NULL,
		NULL,
		&PrevNot
	);

	PWLAN_BSS_LIST wlanList;
	if (WlanGetNetworkBssList(m_wlanHandle, &m_wlanGUID.InterfaceGuid, NULL, dot11_BSS_type_any, 0, NULL, &wlanList) != 0)
	{
		throw std::runtime_error("WlanGetNetworkBssListError");
	}

	WiFiPoint out("", "", "", 0, 0);
	for (size_t i = 0; i < wlanList->dwNumberOfItems; i++)
	{
		std::string readSSID = "";
		for (size_t sizeSSID = 0; sizeSSID < wlanList->wlanBssEntries[i].dot11Ssid.uSSIDLength; sizeSSID++)
		{
			readSSID = readSSID + (char)wlanList->wlanBssEntries[i].dot11Ssid.ucSSID[sizeSSID];
		}

		std::string readBSSID = "";
		for (size_t sizeBSSID = 0; sizeBSSID < 6; sizeBSSID++)
		{
			readBSSID = readBSSID + (char)wlanList->wlanBssEntries[i].dot11Bssid[sizeBSSID];
		}

		if (MAC == readBSSID)
		{
			out.SSID = readSSID;
			out.MAC = readBSSID;
			out.freq = wlanList->wlanBssEntries[i].ulChCenterFrequency;
			out.PWR = wlanList->wlanBssEntries[i].lRssi;
			out.visibleMAC = ConvertDECToHEX(out.MAC);

			break;
		}
	}

	WlanFreeMemory(wlanList);

	if (out.PWR == 0)
	{
		throw std::runtime_error("GetInfoWiFiError");
	}

	return out;
}

void Wlan::WiFiNotify(WLAN_NOTIFICATION_DATA* wlanNotifyData)
{
	if (wlanNotifyData->NotificationCode == wlan_notification_acm_scan_complete)
	{
		LockThread = false;
	}
	else if (wlanNotifyData->NotificationCode == wlan_notification_acm_scan_fail)
	{
		LockThread = false;
		//throw std::runtime_error("Scan fail");
	}
}

std::string Wlan::ConvertDECToHEX(std::string dec)
{
	std::string out = "";
	std::string outHex1 = "";
	std::string outHex2 = "";

	int byte = 0;
	int hex1 = 0;
	int hex2 = 0;

	for (size_t i = 0; i < dec.size(); i++)
	{
		if ((int)dec[i] < 0)
		{
			byte = 256 + (int)dec[i];
		}
		else
		{
			byte = (int)dec[i];
		}

		hex1 = byte % 16;
		hex2 = byte / 16;

		switch (hex1)
		{
		case 10:
			outHex1 = "A";
			break;
		case 11:
			outHex1 = "B";
			break;
		case 12:
			outHex1 = "C";
			break;
		case 13:
			outHex1 = "D";
			break;
		case 14:
			outHex1 = "E";
			break;
		case 15:
			outHex1 = "F";
			break;
		default:
			outHex1 = std::to_string(hex1);
			break;
		}

		switch (hex2)
		{
		case 10:
			outHex2 = "A";
			break;
		case 11:
			outHex2 = "B";
			break;
		case 12:
			outHex2 = "C";
			break;
		case 13:
			outHex2 = "D";
			break;
		case 14:
			outHex2 = "E";
			break;
		case 15:
			outHex2 = "F";
			break;
		default:
			outHex2 = std::to_string(hex2);
			break;
		}

		if (i + 1 != dec.size())
		{
			out = out + outHex2 + outHex1 + ":";
		}
		else
		{
			out = out + outHex2 + outHex1;
		}
	}

	if (out == "")
	{
		out = " ";
		//throw std::runtime_error("Conversion error");
	}

	return out;
}