﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#pragma once

#include <vector>

#include <wx/app.h>

#include <windows.h>
#include <wlanapi.h>

namespace DetectPositioningWiFi
{
	class Wlan
	{
	public:
		struct WiFiPoint
		{
			WiFiPoint(std::string inputSSID, std::string inputMAC, std::string _visibleMAC, double inputFreq, int inputPWR)
			{
				SSID = inputSSID;
				MAC = inputMAC;
				visibleMAC = _visibleMAC;
				freq = inputFreq;
				PWR = inputPWR;
			}
			std::string SSID;
			std::string MAC;
			std::string visibleMAC;
			double freq;
			int PWR;
		};

		static std::vector <std::wstring> GetListAdapters();
		static std::vector <Wlan::WiFiPoint> GetListWiFi(std::wstring inputAdapter);

		Wlan() {};
		Wlan(std::wstring inputAdapter);
		~Wlan();

		void Create(std::wstring inputAdapter);
		WiFiPoint GetInfoWiFi(std::string MAC);

	protected:
		HANDLE m_wlanHandle;
		WLAN_INTERFACE_INFO m_wlanGUID;

		inline static void SmartSleep(size_t time, size_t interval)
		{
			size_t iter = time / interval;

			for (size_t i = 0; i < iter; i++)
			{
				wxApp::GetInstance()->Yield();
				Sleep(interval);
			}
		};

		static bool LockThread;
		static void WiFiNotify(WLAN_NOTIFICATION_DATA *wlanNotifyData);

		static std::string ConvertDECToHEX(std::string dec);
	};
}