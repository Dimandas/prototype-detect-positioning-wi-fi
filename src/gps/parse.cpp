﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "parse.h"

#include <stdexcept>
#include <cmath>

#include "../../3rdparty/minmea/minmea.h"

using namespace DetectPositioningWiFi;

ParseNMEA::ParseNMEA(std::string input)
{
	//struct minmea_sentence_rmc rmcFrame;
	struct minmea_sentence_gga ggaFrame;
	//struct minmea_sentence_gll gllFrame;

	bool ggaParseSucces = false;

	switch (minmea_sentence_id(input.c_str(), false))
	{
	case MINMEA_SENTENCE_GGA:
	{
		ggaParseSucces = minmea_parse_gga(&ggaFrame, input.c_str());
		break;
	}
	}

	if (ggaParseSucces == true)
	{
		// Не работает с -ffast-math
		if (std::isnan(minmea_tocoord(&ggaFrame.latitude)) == true || std::isnan(minmea_tocoord(&ggaFrame.longitude)) == true || std::isnan(minmea_tocoord(&ggaFrame.altitude)) == true)
			throw std::runtime_error("NaN");

		m_latitude = minmea_tocoord(&ggaFrame.latitude);
		m_longitude = minmea_tocoord(&ggaFrame.longitude);
		m_altitude = minmea_tocoord(&ggaFrame.altitude);
	}
	else
	{
		throw std::runtime_error("ParseErrorNMEA");
	}
}

double ParseNMEA::GetGPSLatitude()
{
	return m_latitude;
}

double ParseNMEA::GetGPSLongitude()
{
	return m_longitude;
}

double ParseNMEA::GetGPSAltitude()
{
	return m_altitude;
}