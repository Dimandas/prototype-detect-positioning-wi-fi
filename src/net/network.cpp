﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "../gui/main.h"

#include "../math/trilateration.h"

#include <wx/msgdlg.h>

using namespace DetectPositioningWiFi;

const std::string packetHeader = "Jm04fPi6";
const size_t sizeBuf = 1024;

void MainFrame::SendData(std::string ip, std::string port, double latitude, double longitude, double altitude, double wifiPWR, double wifiFreq, std::string wifiVisibleMAC, std::string wifiSSID)
{
	wxCharBuffer buffer = ((wxString)(packetHeader + "$" + std::to_string(latitude) + "$" + std::to_string(longitude) + "$" + std::to_string(altitude) + "$" + std::to_string(wifiPWR) + "$" + std::to_string(wifiFreq) + "$" + wifiVisibleMAC + "$" + wifiSSID)).ToUTF8();
	size_t txn = buffer.length() + 1;

	wxIPV4address addrLocal;
	addrLocal.AnyAddress();
	wxDatagramSocket sock2tx(addrLocal);

	wxIPV4address raddr;
	raddr.Hostname(ip);
	raddr.Service(port);

	if (sock2tx.SendTo(raddr, buffer.data(), txn).LastCount() != txn)
	{
		//throw std::runtime_error("SendDataError");
	}
}

void MainFrame::OnSocketEvent1(wxSocketEvent& event)
{
	wxIPV4address addr;
	addr.AnyAddress();

	char buf[sizeBuf];
	size_t n;

	switch (event.GetSocketEvent())
	{
	case wxSOCKET_INPUT:
		sock1->Notify(false);

		wxDatagramSocket& inputSocket = sock1->RecvFrom(addr, buf, sizeof(buf));
		n = inputSocket.LastCount();
		if (n == 0)
		{
			sock1->Notify(true);
			//throw std::runtime_error("ErrorRecv");
			return;
		}

		double latitude;
		double longitude;
		double altitude;
		std::string ssid;
		std::string wifiVisibleMAC;
		double wifiPWR;
		double wifiFreq;

		try
		{
			buf[sizeBuf - 1] = '\0';

			char* token = std::strtok(buf, "$");
			std::string ident = "";

			ident = token;
			if (ident != packetHeader)
			{
				sock1->Notify(true);
				//throw std::runtime_error("NotNetworkPacket");
				return;
			}

			token = std::strtok(NULL, "$");
			latitude = atof(token);

			token = std::strtok(NULL, "$");
			longitude = atof(token);

			token = std::strtok(NULL, "$");
			altitude = atof(token);

			token = std::strtok(NULL, "$");
			wifiPWR = atof(token);

			token = std::strtok(NULL, "$");
			wifiFreq = atof(token);

			token = std::strtok(NULL, "$");
			wifiVisibleMAC = token;

			token = std::strtok(NULL, "$");
			ssid = token;
		}
		catch (...)
		{
			sock1->Notify(true);
			//throw std::runtime_error("ErrorParseNet");
			return;
		}

		if (m_ipPortServer == "")
		{
			wxIPV4address inputAddr;
			inputSocket.GetPeer(inputAddr);
			m_ipPortComp1 = inputAddr.IPAddress();
		}

		m_latitude1 = latitude;
		m_longitude1 = longitude;
		m_altitude1 = altitude;
		m_SSID1 = ssid;
		m_wifiVisibleMAC1 = wifiVisibleMAC;
		m_wifiPWR1 = wifiPWR;
		m_coordUsed1 = true;

		try
		{
			m_wifiDistance1 = Trilateration::CalculateDistance(wifiPWR, wifiFreq);
		}
		catch (...)
		{
			m_wifiDistance1 = -1;
		}

		m_ipStatus1 = " (OK)";

		m_updateLabel();

		sock1->Notify(true);
		break;
	}
}

void MainFrame::OnSocketEvent2(wxSocketEvent& event)
{
	wxIPV4address addr;
	addr.AnyAddress();

	char buf[sizeBuf];
	size_t n;

	switch (event.GetSocketEvent())
	{
	case wxSOCKET_INPUT:
		sock2->Notify(false);

		wxDatagramSocket& inputSocket = sock2->RecvFrom(addr, buf, sizeof(buf));
		n = inputSocket.LastCount();
		if (n == 0)
		{
			sock2->Notify(true);
			//throw std::runtime_error("ErrorRecv");
			return;
		}

		double latitude;
		double longitude;
		double altitude;
		std::string ssid;
		std::string wifiVisibleMAC;
		double wifiPWR;
		double wifiFreq;

		try
		{
			buf[sizeBuf - 1] = '\0';

			char* token = std::strtok(buf, "$");
			std::string ident = "";

			ident = token;
			if (ident != packetHeader)
			{
				sock2->Notify(true);
				//throw std::runtime_error("NotNetworkPacket");
				return;
			}

			token = std::strtok(NULL, "$");
			latitude = atof(token);

			token = std::strtok(NULL, "$");
			longitude = atof(token);

			token = std::strtok(NULL, "$");
			altitude = atof(token);

			token = std::strtok(NULL, "$");
			wifiPWR = atof(token);

			token = std::strtok(NULL, "$");
			wifiFreq = atof(token);

			token = std::strtok(NULL, "$");
			wifiVisibleMAC = token;

			token = std::strtok(NULL, "$");
			ssid = token;
		}
		catch (...)
		{
			sock2->Notify(true);
			//throw std::runtime_error("ErrorParseNet");
			return;
		}

		if (m_ipPortServer == "")
		{
			wxIPV4address inputAddr;
			inputSocket.GetPeer(inputAddr);
			m_ipPortComp2 = inputAddr.IPAddress();
		}

		m_latitude2 = latitude;
		m_longitude2 = longitude;
		m_altitude2 = altitude;
		m_SSID2 = ssid;
		m_wifiVisibleMAC2 = wifiVisibleMAC;
		m_wifiPWR2 = wifiPWR;
		m_coordUsed2 = true;

		try
		{
			m_wifiDistance2 = Trilateration::CalculateDistance(wifiPWR, wifiFreq);
		}
		catch (...)
		{
			m_wifiDistance2 = -1;
		}

		m_ipStatus2 = " (OK)";

		m_updateLabel();

		sock2->Notify(true);
		break;
	}
}

void MainFrame::OnSocketEvent3(wxSocketEvent& event)
{
	wxIPV4address addr;
	addr.AnyAddress();

	char buf[sizeBuf];
	size_t n;

	switch (event.GetSocketEvent())
	{
	case wxSOCKET_INPUT:
		sock3->Notify(false);

		wxDatagramSocket& inputSocket = sock3->RecvFrom(addr, buf, sizeof(buf));
		n = inputSocket.LastCount();
		if (n == 0)
		{
			sock3->Notify(true);
			//throw std::runtime_error("ErrorRecv");
			return;
		}

		double latitude;
		double longitude;
		double altitude;
		std::string ssid;
		std::string wifiVisibleMAC;
		double wifiPWR;
		double wifiFreq;

		try
		{
			buf[sizeBuf - 1] = '\0';

			char* token = std::strtok(buf, "$");
			std::string ident = "";

			ident = token;
			if (ident != packetHeader)
			{
				sock3->Notify(true);
				//throw std::runtime_error("NotNetworkPacket");
				return;
			}

			token = std::strtok(NULL, "$");
			latitude = atof(token);

			token = std::strtok(NULL, "$");
			longitude = atof(token);

			token = std::strtok(NULL, "$");
			altitude = atof(token);

			token = std::strtok(NULL, "$");
			wifiPWR = atof(token);

			token = std::strtok(NULL, "$");
			wifiFreq = atof(token);

			token = std::strtok(NULL, "$");
			wifiVisibleMAC = token;

			token = std::strtok(NULL, "$");
			ssid = token;
		}
		catch (...)
		{
			sock3->Notify(true);
			//throw std::runtime_error("ErrorParseNet");
			return;
		}

		if (m_ipPortServer == "")
		{
			wxIPV4address inputAddr;
			inputSocket.GetPeer(inputAddr);
			m_ipPortComp3 = inputAddr.IPAddress();
		}

		m_latitude3 = latitude;
		m_longitude3 = longitude;
		m_altitude3 = altitude;
		m_SSID3 = ssid;
		m_wifiVisibleMAC3 = wifiVisibleMAC;
		m_wifiPWR3 = wifiPWR;
		m_coordUsed3 = true;

		try
		{
			m_wifiDistance3 = Trilateration::CalculateDistance(wifiPWR, wifiFreq);
		}
		catch (...)
		{
			m_wifiDistance3 = -1;
		}

		m_ipStatus3 = " (OK)";

		m_updateLabel();

		sock3->Notify(true);
		break;
	}
}

void MainFrame::OnSocketEvent4(wxSocketEvent& event)
{
	wxIPV4address addr;
	addr.AnyAddress();

	char buf[sizeBuf];
	size_t n;

	switch (event.GetSocketEvent())
	{
	case wxSOCKET_INPUT:
		sock4->Notify(false);

		wxDatagramSocket& inputSocket = sock4->RecvFrom(addr, buf, sizeof(buf));
		n = inputSocket.LastCount();
		if (n == 0)
		{
			sock4->Notify(true);
			//throw std::runtime_error("ErrorRecv");
			return;
		}

		double latitude;
		double longitude;
		double altitude;
		std::string ssid;
		std::string wifiVisibleMAC;
		double wifiPWR;
		double wifiFreq;

		try
		{
			buf[sizeBuf - 1] = '\0';

			char* token = std::strtok(buf, "$");
			std::string ident = "";

			ident = token;
			if (ident != packetHeader)
			{
				sock4->Notify(true);
				//throw std::runtime_error("NotNetworkPacket");
				return;
			}

			token = std::strtok(NULL, "$");
			latitude = atof(token);

			token = std::strtok(NULL, "$");
			longitude = atof(token);

			token = std::strtok(NULL, "$");
			altitude = atof(token);

			token = std::strtok(NULL, "$");
			wifiPWR = atof(token);

			token = std::strtok(NULL, "$");
			wifiFreq = atof(token);

			token = std::strtok(NULL, "$");
			wifiVisibleMAC = token;

			token = std::strtok(NULL, "$");
			ssid = token;
		}
		catch (...)
		{
			sock4->Notify(true);
			//throw std::runtime_error("ErrorParseNet");
			return;
		}

		if (m_ipPortServer == "")
		{
			wxIPV4address inputAddr;
			inputSocket.GetPeer(inputAddr);
			m_ipPortComp4 = inputAddr.IPAddress();
		}

		m_latitude4 = latitude;
		m_longitude4 = longitude;
		m_altitude4 = altitude;
		m_SSID4 = ssid;
		m_wifiVisibleMAC4 = wifiVisibleMAC;
		m_wifiPWR4 = wifiPWR;
		m_coordUsed4 = true;

		try
		{
			m_wifiDistance4 = Trilateration::CalculateDistance(wifiPWR, wifiFreq);
		}
		catch (...)
		{
			m_wifiDistance4 = -1;
		}

		m_ipStatus4 = " (OK)";

		m_updateLabel();

		sock4->Notify(true);
		break;
	}
}