﻿/*
   Copyright 2019 Dimandas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "app.h"
#include "gui/main.h"

using namespace DetectPositioningWiFi;

wxIMPLEMENT_APP(ProjectApp);

#include <wx/display.h>

bool ProjectApp::OnInit()
{
	wxRect tempObject = wxDisplay((unsigned int)0).GetClientArea();
	tempObject.width = tempObject.width * 0.90;
	tempObject.height = tempObject.height * 0.78;

	MainFrame *FrameMain = new MainFrame(0, wxID_ANY, wxT("Prototype Detect Position Wi-Fi"), wxDefaultPosition, wxSize(tempObject.width, tempObject.height));
	FrameMain->Show(true);
	return true;
}